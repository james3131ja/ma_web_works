<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" version="1.0" standalone="yes" indent="yes" />
<xsl:template match="/">


<HTML>
<HEAD><TITLE>MA Assignment</TITLE>
<style>
tr td{
border:solid black 1px;
}

td{
text-align:center;
padding:3px;
background-color:grey;
}
</style>
</HEAD>
<BODY>
<xsl:apply-templates select="PLAY" />
</BODY>
</HTML>
</xsl:template > 

<xsl:variable name="number">
  <xsl:number/>
</xsl:variable>

<xsl:template match="PLAY">
<h1>Speeches</h1>
<table>
<tr><th>Num</th><th>Speaker</th> <th>Speech</th></tr>
<xsl:for-each select="ACT/SCENE/SPEECH [ SPEAKER='LORD POLONIUS' ]">
<tr>
<td>
<xsl:number value="position()" format="1. "/>
</td>
<td><xsl:value-of select="SPEAKER" /> </td>
<td>
<xsl:for-each select="LINE">
<xsl:value-of select="text()" /><br/> 
</xsl:for-each>
</td>
</tr>
</xsl:for-each>
</table>
</xsl:template>
</xsl:stylesheet>