﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class _Default : System.Web.UI.Page
{

    XmlDocument doc;
  


    public void Page_Load(object sender, EventArgs e)
    {
        
    }

    public XmlDocument openXMLDoc(){
        this.doc = new XmlDocument();
        return this.doc;
    }

    public void createDoc()
    {
        //create the declaration section
        this.doc = new XmlDocument();
        XmlDeclaration newDec = doc.CreateXmlDeclaration("1.0", null, null);
        doc.AppendChild(newDec);
        XmlElement newRoot = doc.CreateElement("timetable");
        doc.AppendChild(newRoot);
        XmlElement newStudent = doc.CreateElement("student");
        XmlElement fname = doc.CreateElement("first_name");
        XmlElement lname = doc.CreateElement("last_name");
        XmlElement student_id = doc.CreateElement("student_id");
        XmlElement campus = doc.CreateElement("campus");

        fname.InnerText = "James";
        lname.InnerText = "LUO";
        student_id.InnerText = "n01004222";
        campus.InnerText = "Humber North";

        newRoot.AppendChild(fname);
        newRoot.AppendChild(lname);
        newRoot.AppendChild(student_id);
        newRoot.AppendChild(campus);

        //another complex type
        XmlElement daysofweek = doc.CreateElement("daysofweek");
        newRoot.AppendChild(daysofweek);

        XmlElement day2 = doc.CreateElement("day");
        XmlAttribute day2attr = doc.CreateAttribute("day_name");
        day2attr.InnerText = "Tue";
        day2.Attributes.Append(day2attr);
        XmlElement courses2 = doc.CreateElement("courses");
        day2.AppendChild(courses2);

        XmlElement day3 = doc.CreateElement("day");
        XmlAttribute day3attr = doc.CreateAttribute("day_name");
        day3attr.InnerText = "Wed";
        day3.Attributes.Append(day3attr);
        XmlElement courses3 = doc.CreateElement("courses");
        day3.AppendChild(courses3);

        XmlElement day4 = doc.CreateElement("day");
        XmlAttribute day4attr = doc.CreateAttribute("day_name");
        day4attr.InnerText = "Thu";
        day4.Attributes.Append(day4attr);
        XmlElement courses4 = doc.CreateElement("courses");
        day4.AppendChild(courses4);

        XmlElement day5 = doc.CreateElement("day");
        XmlAttribute day5attr = doc.CreateAttribute("day_name");
        day5attr.InnerText = "Fri";
        day5.Attributes.Append(day5attr);
        XmlElement courses5 = doc.CreateElement("courses");
        day5.AppendChild(courses5);

        daysofweek.AppendChild(day2);
        daysofweek.AppendChild(day3);
        daysofweek.AppendChild(day4);
        daysofweek.AppendChild(day5);
        //count_times++;
        writeToXMl();
        
    }

    public void btnSubmit_Click(object sender, EventArgs e)
    {
        openXMLDoc();
        try
        {
            doc.Load("C:\\Users\\ma\\Desktop\\WebSite2\\users.xml");
            //doc.Load(".\\users.xml");
            XmlNode courses2 = doc.ChildNodes[1].ChildNodes[4].ChildNodes[0].ChildNodes[0];
            XmlNode courses3 = doc.ChildNodes[1].ChildNodes[4].ChildNodes[1].ChildNodes[0];
            XmlNode courses4 = doc.ChildNodes[1].ChildNodes[4].ChildNodes[2].ChildNodes[0];
            XmlNode courses5 = doc.ChildNodes[1].ChildNodes[4].ChildNodes[3].ChildNodes[0];
            XmlNode[] coursesArray = { courses2, courses3, courses4, courses5 };

            if (cbTue.Checked == true)
            {
                coursesArray[0].AppendChild(prepareCourseElement());

            }
            else if (cbWed.Checked == true)
            {
                coursesArray[1].AppendChild(prepareCourseElement());
            }
            else if (cbThu.Checked == true)
            {
                coursesArray[2].AppendChild(prepareCourseElement());

            }
            else if (cbFri.Checked == true)
            {
                coursesArray[3].AppendChild(prepareCourseElement());
            }

            writeToXMl();
        }
        catch
        {
            createDoc();
        }
        finally {

           
        }

    }

    public XmlElement prepareCourseElement()
    {
        XmlElement course = doc.CreateElement("course");
        XmlElement course_name = doc.CreateElement("course_name");
        course_name.InnerText = TextBox5.Text;
        XmlElement course_code = doc.CreateElement("course_code");
        course_code.InnerText = TextBox6.Text;
        XmlElement instructor = doc.CreateElement("instructor");
        instructor.InnerText = TextBox7.Text;
        XmlElement place = doc.CreateElement("place");
        place.InnerText = TextBox8.Text;
        XmlElement starting_time = doc.CreateElement("starting_time");
        starting_time.InnerText = TextBox9.Text;
        XmlElement end_time = doc.CreateElement("end_time");
        end_time.InnerText = TextBox10.Text;
        XmlElement time = doc.CreateElement("time");
        course.AppendChild(course_name);
        course.AppendChild(course_code);
        course.AppendChild(instructor);
        course.AppendChild(place);
        course.AppendChild(time);
        time.AppendChild(starting_time);
        time.AppendChild(end_time);
        
        return course;
    }

    public void writeToXMl()
    {
        XmlTextWriter tr = new XmlTextWriter("C:\\Users\\ma\\Desktop\\WebSite2\\users.xml", null);
        //XmlTextWriter tr = new XmlTextWriter(".\\users.xml", null);
        tr.Formatting = Formatting.Indented;
        doc.WriteContentTo(tr);
        tr.Close();
    }
}
    