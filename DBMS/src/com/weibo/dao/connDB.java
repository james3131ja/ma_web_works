package com.weibo.dao;

import java.sql.*;
import java.io.*;
import java.util.*;

public class connDB {
  /*操作数*/
  public Connection conn = null;
  public PreparedStatement stmt = null;
  public ResultSet rs = null;
  Statement statement = null;
  
  private static String dbClassName ="com.mysql.jdbc.Driver";
  private static String dbUrl ="jdbc:mysql://localhost:3306/weibo";
  private static String dbUser = "root";
  private static String dbPwd = "mysql";

  /*no arguments -- defaultly execute*/
  public  Connection getConnection() {
    try {
      Class.forName(dbClassName);
      conn = DriverManager.getConnection(dbUrl, dbUser, dbPwd);
      statement = conn.createStatement();
    }
    catch (ClassNotFoundException e){
		System.out.println("NotDriver.");
	}catch (SQLException e){
		System.out.println("SQLExcption.");
	}
    return conn;
  }


  /*search查询*/
  public ResultSet executeQuery(String sql) {
    try {
      conn = getConnection();
      rs = statement.executeQuery(sql);
    }
    catch (SQLException ex) {
    	ex.printStackTrace();
    	System.err.println(ex.getMessage());
    }
    return rs;
  }

  /*update更新*/
  public int executeUpdate(String sql) {
    int result = 1;
    try {
      conn = getConnection();
      statement.executeUpdate(sql);
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    	result = 0;
    }
    try {
    	statement.close();
    }
    catch (SQLException ex1) {
    }
    return result;
  }


  /*
  public int executeUpdate_id(String sql) {
    int result = 0;
    try {
      conn = getConnection();
      stmt = conn.prepareStatement(sql);
      result = stmt.executeUpdate(sql);
      String ID = "select @@IDENTITY as id";
      rs = stmt.executeQuery(ID);
      if (rs.next()) {
        int autoID = rs.getInt("id");
        result = autoID;
      }
    }
    catch (SQLException ex) {
      result = 0;
    }
    return result;
  }
  */	
  
  
  /*close connection*/

  public void close() {
    try {
      if (rs != null) {
        rs.close();
      }
    }
    catch (Exception e) {
      e.printStackTrace(System.err);
    }
    try {
      if (statement != null) {
        statement.close();
      }
    }
    catch (Exception e) {
      e.printStackTrace(System.err);
    }
    try {
      if (conn != null) {
        conn.close();
      }
    }
    catch (Exception e) {
      e.printStackTrace(System.err);
    }
  }

}
