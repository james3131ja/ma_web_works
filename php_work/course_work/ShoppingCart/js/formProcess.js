function updateItem() {
    var formData = {
        'name': $('input[name=name]').val(),
        'price': $('input[name=price]').val(),
        'category': $('input[name=category]').val(),
        'code': $('input[name=code]').val()
    };
       $.ajax({
        type: 'post',
        url: 'serviceUpdateItem.php',
        data: {'formObj': formData},
        dataType: 'json',
        error: function () {
            //alert("error");
            alert("Update Successfull");
            window.location.reload();
        },
        success: function (result) {
            alert("Update Successfull");
            window.location.reload();
        }
    });
}

function getActivated(clicked_id) {
    $("ul > li").each(function () {
        $(this).removeClass("active");
    });
    $("#" + clicked_id).addClass("active");
}

function selectItemFromCart(clicked_id) {
     //alert(clicked_id);
     $.ajax({
        type: 'post',
        url: 'serviceSelectItemFromCart.php',
        data: {'Id': clicked_id},
        dataType: 'json',
        error: function (result) {
            alert(result.msg);
            //window.location.reload();
        },
        success: function (result) {
            alert(result.Id);
        }
    });
    window.location.href = 'http://localhost/Lab1/ShoppingCart/index.php';
}

function deleteItemFromCart(clicked_id) {
    //alert("I am here");
    $.ajax({
        type: 'post',
        url: 'serviceDeleteFromCart.php',
        data: {'Id': clicked_id},
        dataType: 'json',
        error: function (result) {
            alert(result.error);
            window.location.reload();
        },
        success: function (result) {
            alert(result.msg);
            $('.table').load(document.URL + ' .table');
        }
    });
}

function showCart(){
    window.location.href = 'http://localhost/Lab1/ShoppingCart/frmItemsList.php';
}

function backToForm(){
    window.location.href = 'http://localhost/Lab1/ShoppingCart/index.php';
}