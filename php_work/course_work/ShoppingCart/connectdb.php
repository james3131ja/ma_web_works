<?php

class DBConn {

    private $username = "root";
    private $password = "";
    private $dsn = "mysql:host=localhost;dbname=";
    private $con = null;

    public function __construct($dbName) {
        $this->dsn .= $dbName;
        $this->con = new PDO($this->dsn, $this->username, $this->password);
    }

    public function destroyDB() {
        $this->con = null;
    }
    
    public function getDb(){
        
        return $this->con;
    }

}
?>