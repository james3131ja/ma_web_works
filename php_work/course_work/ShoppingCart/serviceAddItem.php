<?php
require 'connectdb.php';
require 'shoppingCartBO.php';
$code = isset($_POST["code"])?$_POST["code"]:null;
$name = isset($_POST["name"])?$_POST["name"]:null;
$price = isset($_POST["price"])?$_POST["price"]:null;
$category = isset($_POST["category"])?$_POST["category"]:null;

$db = new DBConn("wordpress");
$cartBO = new shoppingCartBO($db->getDb());
$cartBO->addToCart($name, $code, $price, $category);
$db->destroyDB();

//put validations here
echo 'Item added to your cart';
header("refresh:2; url=frmItemsList.php");
?>