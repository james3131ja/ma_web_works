<?php

class shoppingCartBO{
    private $con=null;
    
    public function __construct($db) {
        $this->con = $db;
    }
    
    public function deleteById($Id) {
        try {
            $deletesql = "DELETE FROM shoppingcart where item_id = :Id";
            $stmt = $this->con->prepare($deletesql);
            $stmt->bindParam(":Id", intval($Id));
            $result = $stmt->execute();
            //echo 'delete '.$result;
        } catch (PDOException $ex) {
            
        }
    }
    
    public function getCart(){
        try{
        $sql = "SELECT * FROM shoppingcart";
        $results = $this->con->query($sql);    
        return $results;    
        }catch(Exception $ex){}
    }
    
    
    public function addToCart($name, $code, $price, $category) {
        try {
            $insert = "INSERT INTO shoppingcart(name,code,price,category)VALUES(:name,:code,:price,:category)";
            $stmt = $this->con->prepare($insert);
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':code', $code);
            $stmt->bindParam(':price', $price);
            $stmt->bindParam(':category', $category);
            $result = $stmt->execute();
        } catch (PDOException $ex) {
            //echo "Error : " . $ex->getMessage();
        }
    }
   
    public function getById($Id){
          try {
            $sql = "SELECT * FROM shoppingcart where item_id = $Id";
            $result = $this->con->query($sql);
            return $result;
        } catch (PDOException $ex) {
            //echo "Error : " . $ex->getMessage();
        }
    }
    
    public function updateItemByCode($code,$name,$price,$category){
        try {
            $sql = "UPDATE shoppingcart SET name=:name,price=:price,category=:category where code=:code";
            $stmt = $this->con->prepare($sql);
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':price', $price);
            $stmt->bindParam(':category', $category);
            $stmt->bindParam(':code',$code);
            $result = $stmt->execute();
            echo $result;
        } catch (PDOException $ex) {
            echo "Error : " . $ex->getMessage();
        }
    }
}

?>

