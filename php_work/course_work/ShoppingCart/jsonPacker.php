<?php
require 'connectdb.php'; 
require 'shoppingCartBO.php';

class JsonPacker{
    
function deleteItemByIdPacker($Id) {
    $result['msg'] = ''; //detail message
    $result['error'] = false; //true of false
    $result['error_message'] = ''; //exceptions 
    if (!isset($Id) || empty($Id)) {
        $result['error'] = true;
        $result['msg'] .= 'Error: Id is empty !';
    }
    if ($result['error'] === false) {
        $result['msg'] .= 'Delete Successful !';
        $Id = substr($Id, 3);
         try {
            $db = new DBConn("wordpress");
            $cartBo = new shoppingCartBO($db->getDb());
            $cartBo->deleteById($Id);
            $db->destroyDB();
        } catch (Exception $ex) {
           $result["error_message"] .= $ex->getMessage();
        }
    }
    return $result;
}

    function updateItemByCode($form){
        $code = $form["code"];
        $name = $form['name'];
        $price = $form["price"];
        $category = $form["category"];
        
        $result['msg'] = 'fail';
        try{
            $db = new DBConn("wordpress");
            $BO = new shoppingCartBO($db->getDb());
            $BO->updateItemByCode($code, $name, $price, $category);
            $db->destroyDB();
            $result["msg"]="updated";
        }catch(Exception $ex){
            $result["msg"]="exception";
        }
        return $result;
    }
    
    function selectFromCart($Id){
         $result['msg'] = 'fail';
         try{
           session_start();
           $_SESSION["item_code"]=substr($Id,6);
           $result["Id"]=substr($Id,6);
           $result['msg']='success';
         }catch(Exception $ex){
             $result['exception']="exception";
         }
        return $result;
    }
}
