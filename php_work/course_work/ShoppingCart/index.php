<?php
$code = "";
$name = "";
$price = "";
$category = "";
session_start();
require_once 'connectdb.php';
require_once 'shoppingCartBO.php';
$db = new DBConn("wordpress");
$BO = new shoppingCartBO($db->getDb());
$obj = $BO->getById(intval(isset($_SESSION["item_code"]) ? $_SESSION["item_code"] : "0"));
if(isset($_SESSION["item_code"]))
foreach ($obj as $row) {
    $code = $row["code"];
    $name = $row["name"];
    $category = $row["category"];
    $price = $row["price"];
}
session_destroy();
?>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="./css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="./css/main.css" />
        <script src="./js/formProcess.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js "></script>
    </head>
    <body>
        <div id = "container">
            <form id="orderForm" role="form" action="serviceAddItem.php" method="post">
                <div class="form-group">
                    <label for="code">Product Code :</label>
                    <?php
                    echo "<input name='code' type='text' class='form-control' value='$code' placeholder='Enter Code'>";
                    ?>
                </div>
                <div class="form-group">
                    <label for="name">Name :</label>
                    <?php
                    echo "<input name='name' type='text' class='form-control' value='$name' placeholder='Enter Name'>";
                    ?>
                </div>
                <div class="form-group">
                    <label for="price">Price :</label>
                    <?php
                    echo "<input name='price' type='text' class='form-control' value='$price' placeholder='Enter Price'>";
                    ?>
                </div>
                <div class="form-group">
                    <label for="category">Category :</label>
                    <?php
                    echo "<input name='category' type='text' class='form-control'  value='$category' placeholder='Enter Category'>";
                    ?>
                </div>

                <button type="submit" class="btn btn-default">Add</button>
                <button type="button" class="btn btn-default" onclick="updateItem();">Update</button>
                <button type="button" class="btn btn-default" onclick="showCart();">Show Cart</button>
            </form>
        </div>   
    </body>
</html>
