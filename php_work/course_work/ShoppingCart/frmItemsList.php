<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="./css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="./css/main.css" />
        <!--<script src="./js/jquery-1.11.1.min"></script>-->
        <script src="./js/formProcess.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js "></script>
        <title></title>
    </head>
    <body>

        <?php
        require 'connectdb.php';
        require 'shoppingCartBO.php';
        $db = new DBConn("wordpress");
        $cartBO = new shoppingCartBO($db->getDb());
        $result = $cartBO->getCart();
        ?>
        <table class='table'><thead>
                <tr>
                    <th>Code</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Category</th>
                    <th><input type='button' value='New Item' onclick='backToForm()'></th>
                </tr>
            </thead>        

            <?php
            foreach ($result as $raw) {
                ?>
                <tr>
                    <td><?= $raw["code"] ?></td>
                    <td><?= $raw["name"] ?></td>
                    <td><?= $raw["price"] ?></td>
                    <td><?= $raw["category"] ?></td>
                    <?php
                    echo "<td><input type='button' value='select' id=select" . $raw['item_id'] . " onclick='selectItemFromCart(this.id)'>" . "</td>";
                    echo "<td><input type='button' value='delete' id=btn" . $raw['item_id'] . " onclick='deleteItemFromCart(this.id)'>" . "</td>";
                }
                $db->destroyDB();
                ?>
            </tr>
        </table>
    </body>
</html>