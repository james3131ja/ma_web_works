<?php

//gathering values of the form and store it into database
$dsn = 'mysql:host=localhost:3306;dbname=wordpress';
$username = "root";
$password = "";
$query = "INSERT INTO phplab (name,password,email) VALUES (:name,:password,:email)";


//testing insert method in PDO
try {
    //PDO Access database
    /*
      $db = new PDO($dsn, $username, $password);
      $stmt = $db->prepare($query);
      $stmt->bindParam(':name',$_POST['Name']);
      $stmt->bindParam(":password",$_POST['Passwd']);
      $stmt->bindParam(':email', $_POST['email']);
      $stmt->execute();
     */

    /*
      $dbhost = 'localhost';
      $dbuser = 'root';
      $dbpass = '';
      $dbDatabase='wordpress';
      $conn = mysql_connect($dbhost, $dbuser, $dbpass);
      mysql_select_db($dbDatabase);
      if(! $conn )
      {
      die('Could not connect: ' . mysql_error());
      }

      $sql = 'INSERT INTO phplab '.
      '(name,password, email) '.
      'VALUES (?,?,?)';
      $retval = mysql_query( $sql, $conn );
      if(! $retval )
      {
      die('Could not enter data: ' . mysql_error());
      }
      echo "Entered data successfully\n";
      mysql_close($conn);
     */
} catch (Exception $e) {
    $error_message = $e->getMessage();
    echo "<p>Error Message: $error_message</p>";
}
//go to welcome page
//header( "location:welcome.php?check=$insert_check" ); 
//print_r method -> print the associate array;
//echo "Username: " . $_POST["Name"];
//hints:validation should be both on client side and server side
//should learn regular expression - > will be on the midterm
//validation part
//we can also use method empty() to check the blank fields
//preg_match()2 to 4 argus 
//Lab 3

/*
validate_reg("/^[a-zA-Z]+$/", $_POST['Name'], 'Name');
validate_reg('/^[0-9]+$/', $_POST['Passwd'], 'Password');
validate_reg('/^[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}$/', $_POST['email'], 'Email');
checkSelection();
*/
require  'fields.php';

session_start();
$fields = new Fields();
$fields->addField("name","textbox",$_POST["Name"],"");
$fields->addField("password","password",$_POST["Passwd"],'/^[0-9]+$/');
$fields->addField("email","email",$_POST["email"],'/^[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}$/');

$_SESSION["Name"] = $_POST["Name"];
$_SESSION["Password"] = $_POST["Passwd"];
$_SESSION["Email"] = $_POST["email"];

$country = (isset($_POST['country']))? $_POST['country']:"blank";
$fields->addField("country and city","dropdown",$country,"");
$_SESSION["Country"]=$country;

$sex = (isset($_POST['sex']))? $_POST['sex']:"";
$fields->addField("gender","radiobutton",$sex,"");
$_SESSION["Sex"]=$sex;

$hobby = isset($_POST['Hobby'])?$_POST['Hobby']:"";
$fields->addField("hobby","checkbox",isset($_POST['Hobby'])?$_POST['Hobby'][0]:"","");
$_SESSION["Hobby"]=$hobby;

$err_message = $fields->validate();
$_SESSION["ERR_Message"] = $err_message;

/*
function checkSelection() {
    $err_message = "";
    if ($_POST['Country'] == 'blank') {
        $err_message .= "Please select a country <br/>";
    }

    if (!isset($_POST['sex'])) {
        $err_message .= "Please select a gender<br/>";
    }

    if (
            !(isset($_POST['Basketball']) || isset($_POST['Singing']) || isset($_POST['Photographing']) || isset($_POST['Others']))
    ) {
        $err_message .= "Please select a Hobby <br/>";
    }
    $_SESSION["ERR_Message"] .= $err_message;
}

function validate_reg($pattern, $val, $err) {
    if (isset($val)) {
        if (!preg_match($pattern, $val)) {
            give_err_message("Validation " . $err);
        }
    } else {
        give_err_message($err);
    }
}

function give_err_message($val) {
    $err_message = $val . " Format Wrong </br>";
    $_SESSION["ERR_Message"] .= $err_message;
}
*/

if(strlen($_SESSION["ERR_Message"])!=0){
header("location:index.php?err_message=" . $_SESSION["ERR_Message"]);}
else{
    header("location:welcome.php");
}
?>