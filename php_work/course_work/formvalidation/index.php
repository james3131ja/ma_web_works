<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="./css/bootstrap.css" />
        <!--<link rel="stylesheet" text="text/css" href="./css/main.css" />-->
        <link rel="stylesheet" type="text/css" href="./css/newForm.css" />
        <script src="./js/jquery-1.11.1.min.js"></script>
        <script src="./js/nav.js"></script>
        <title>James Home</title>
    </head>
    <body>
        <div id ="container">
            <?php
            
            include_once 'header.php';
            include_once 'navigation.php';
            include_once 'newForm.php';
            include_once 'footer.php';
            if (isset($_GET['err_message'])) {
                echo "<p>". $_GET["err_message"] ."</p>";
            }
            
           
            ?>
        </div><!--container-->
    </body>
</html>