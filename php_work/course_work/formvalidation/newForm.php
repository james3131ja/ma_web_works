<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$hobby = (isset($_SESSION["Hobby"]))?$_SESSION["Hobby"]:null;
$country = (isset($_SESSION["Country"]))?$_SESSION["Country"]:null;


function checkSelection ($value,$hobby){
$check = false;
    if($hobby!=null)    {
    foreach($hobby as $key){
        if($key==$value){$check=true;}
    }
}
return $check;
}
?>
<div id = "container">
    <form role="form" action="process.php" method="post">
        <div class="form-group">
            <label for="name">Name :</label>
            <input name="Name" type="text" class="form-control" id="name" 
                   placeholder="Enter Name">
        </div>
        <div class="form-group">
            <label for="password">Password :</label>
            <input name="Passwd" type="password" class="form-control" id="password" 
                   placeholder="Enter Password">
        </div>
        <div class="form-group">
            <label for="email">Email :</label>
            <input name="email" type="text" class="form-control" id="email" 
                   placeholder="Enter Email">
        </div>

        <div class="radio">
            <label>Gender :</label>
            <label>
                <input name="sex" type="radio" name="gender" id="radioMale" 
                       value="male">
                Male
            </label>
            <label>
                <input name="sex" type="radio" name="gender" id="radioFemale" 
                       value="female">
                Female
            </label>
        </div>

        <div class="checkbox">
            <label>Hobbies :</label>
            <label>
                <input name = "Hobby[]" type="checkbox"> Basketball
            </label>
            <label>
                <input name = "Hobby[]" type="checkbox"> Singing
            </label>
            <label>
                <input name = "Hobby[]" type="checkbox"> Skiing
            </label>
            <label>
                <input name = "Hobby[]" type="checkbox"> Swimming
            </label>
        </div>
        <div class="form-group">
            <label>Countries & Cities :</label>
            <select name="Country">
                <option value="blank" <?= checkSelection("blank", $hobby) ? "selected" : "" ?> ></option>
                <optgroup label="USA">
                    <option  value="Chicago" <?= (isset($_SESSION["Country"]) && "Chicago" == $_SESSION["Country"]) ? "selected" : "" ?> >Chicago</option>
                    <option  value="New York" <?= (isset($_SESSION["Country"]) && "New York" == $_SESSION["Country"]) ? "selected" : "" ?>>New York</option>
                    <option  value="OthersUS" <?= (isset($_SESSION["Country"]) && "OthersUS" == $_SESSION["Country"]) ? "selected" : "" ?> >OthersUS</option>
                </optgroup>
                <optgroup label="Canada">
                    <option  value="Toronto"  <?= (isset($_SESSION["Country"]) && "Toronto" == $_SESSION["Country"]) ? "selected" : "" ?> >Toronto</option>
                    <option  value="Ottawa" <?= (isset($_SESSION["Country"]) && "Ottawa" == $_SESSION["Country"]) ? "selected" : "" ?> >Ottawa</option>
                    <option  value="OthersCA" <?= (isset($_SESSION["Country"]) && "OthersCA" == $_SESSION["Country"]) ? "selected" : "" ?> >OthersCA</option>   
                </optgroup>
            </select>
        </div>

        <div class="form-group">
            <label for="inputfile">File input :</label>
            <input type="file"  id="inputfile">
        </div>

        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>