<?php


$hobby = (isset($_SESSION["Hobby"]))?$_SESSION["Hobby"]:null;
$country = (isset($_SESSION["Country"]))?$_SESSION["Country"]:null;


function checkSelection ($value,$hobby){
$check = false;
    if($hobby!=null)    {
    foreach($hobby as $key){
        if($key==$value){$check=true;}
    }
}
return $check;
}

?>


<div id ="UserForm">
    <fieldset>
        

        <!--
            get  : passing variables by url
            post : passing data by header
            request : both get and post
        
            bookmark data - get method ???
        
            get works with links -> easier to create a query
            post only work with forms
        
            the default method is get
        -->        

        <form  id="form" action="process.php" method="post">
            <div  id = "UserInput">
                <label>Name:  </label><input type="text" name="Name" id = "Name" value=<?=(isset($_SESSION["Name"]))?$_SESSION["Name"]:""?>/>
                <label>Password:  </label><input type = "password" name="Passwd" id = "Passwd" value=<?=(isset($_SESSION["Password"]))?$_SESSION["Password"]:""?>/>
                <label>Email:  </label><input type = "text" name="email" id = "email" value=<?=(isset($_SESSION["Email"]))?$_SESSION["Email"]:""?>/>
            </div>
            <!--must have the same name-->
            <div  id = "Option">
                <table>
                    <tr>
                        <td>
                            <ul>
                                <li>Gender</li>
                                <li>
                                    <input type="radio"   name="sex" value="Man" <?=(isset($_SESSION["Sex"])&&"Man"==$_SESSION["Sex"])?"checked":""?> />Male
                                    <input  type="radio"  name ="sex" value="Female" <?=(isset($_SESSION["Sex"])&&"Female"==$_SESSION["Sex"])?"checked":""?> />Female
                                </li>
                            </ul>
                        </td>
                        <td rowspan="2">
                            <div class="hobby">Hobbies</div>
                            <input type = "checkbox" name = "Hobby[]" id = "Basketball" value = "Basketball" <?=  checkSelection("Basketball", $hobby)?"checked":"" ?> />Basketball
                            <input type = "checkbox" name = "Hobby[]" id = "Singing" value = "Singing" <?=  checkSelection("Singing", $hobby)?"checked":"" ?> />Singing
                            <input type = "checkbox" name = "Hobby[]" id = "Photographing" value = "Photographing" <?=  checkSelection("Photographing", $hobby)?"checked":"" ?> />Photographing
                            <input type = "checkbox" name = "Hobby[]" id = "Others" value = "Ohters" <?=  checkSelection("Others", $hobby)?"checked":"" ?> />Ohters
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <div class="textmarigncontry" >Country & City</div>
                            <select id="Country" name="country">
                                <option value="blank" <?=  checkSelection("blank", $hobby)?"selected":"" ?> ></option>
                                <optgroup label="USA">
                                    <option  value="Chicago" <?=(isset($_SESSION["Country"])&&"Chicago"==$_SESSION["Country"])?"selected":"" ?> >Chicago</option>
                                    <option  value="New York" <?=(isset($_SESSION["Country"])&&"New York"==$_SESSION["Country"])?"selected":"" ?>>New York</option>
                                    <option  value="OthersUS" <?=(isset($_SESSION["Country"])&&"OthersUS"==$_SESSION["Country"])?"selected":"" ?> >OthersUS</option>
                                </optgroup>
                                <optgroup label="Canada">
                                    <option  value="Toronto"  <?=(isset($_SESSION["Country"])&&"Toronto"==$_SESSION["Country"])?"selected":"" ?> >Toronto</option>
                                    <option  value="Ottawa" <?=(isset($_SESSION["Country"])&&"Ottawa"==$_SESSION["Country"])?"selected":"" ?> >Ottawa</option>
                                    <option  value="OthersCA" <?=(isset($_SESSION["Country"])&&"OthersCA"==$_SESSION["Country"])?"selected":"" ?> >OthersCA</option>   
                                </optgroup>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="divfloatclear"></div>

            <div id = "Comment">
                <div class="textcomment">Comments</div>
                <div><textarea name = "Description" id = "Description" rows ="5" cols="30">
                    </textarea></div>
            </div>

            <div  id ="CheckOut">
                <ul>
                    <li><input type="submit" id="Submit" name="Submit" value="Submit" /></li>
                    <li><input type="reset" id="Reset" name="Reset" value="Reset" /></li>
                </ul>
            </div>

            <div class="divfloatclear"></div>
        </form>
    </fieldset>
    <div class="divfloatclear"></div>
</div><!--form-->

