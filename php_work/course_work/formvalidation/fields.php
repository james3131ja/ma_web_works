<?php

class Fields {

    private $fields = array();
    private $message = array();

    public function __construct() {
        
    }

    //should put a field here
    public function addField($name, $type, $value, $reg) {
        //a standard field with name
        $field = new Field($name, $type, $value, $reg);
        $this->fields[$name] = $field;
    }

    public function getFieldByName($name) {
        return $this->fields[$name];
    }

    public function validate() {
        foreach ($this->fields as $field) {
        //$this->message .= $field->getName() . "<br/>";
            if (strlen($field->getReg()) > 0) {
                if (!preg_match($field->getReg(), $field->getValue())) {
                    $this->message[$field->getName()] = "Please check " . $field->getName() . "<br/>";
                }
            }

            if (strlen($field->getValue()) == 0 || $field->getValue() == 'blank') {
                $this->message[$field->getName()] = "Please fill " . $field->getName() . "<br/>";
            }
        }
        return $this->message;
    }

}

//have to set the fields outside
class Field {

    private $fieldType = "";
    private $value = "";
    private $reg = "";
    private $name = "";

    public function __construct($name, $type, $value, $reg) {
        $this->fieldType = $type;
        $this->value = $value;
        $this->reg = $reg;
        $this->name = $name;
    }

    public function __destruct() {
        
    }

    public function getType() {
        return $this->fieldType;
    }

    public function getValue() {
        return $this->value;
    }

    public function getReg() {
        return $this->reg;
    }

    public function getName() {
        return $this->name;
    }

    public function setType($value) {
        $this->fieldType = $value;
    }

    public function setValue($value) {
        $this->value = $value;
    }

    public function setReg($value) {
        $this->reg = $value;
    }

    public function setName($value) {
        $this->name = $value;
    }

    public function hasReg() {
        return (strlen($this->reg) == 0) ? true : false;
    }

}

?>
