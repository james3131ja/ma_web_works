/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author ma
 */
public class PizzaOrderForm {

    private String[] size;
    private String[] toppings;

    public PizzaOrderForm(String[] size,String[] values) {
        this.size = size;
        this.toppings = values;
    }

    public void setSizeGroup(String[] values) {
        this.size = values;
    }

    public String[] getSizeGroup() {

        return this.size;
    }

    public void setToppingGroup(String[] values) {
        this.toppings = values;
    }

    public String[] getToppingGroup() {

        return this.toppings;
    }

}
