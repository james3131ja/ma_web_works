/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.ArrayList;

/**
 *
 * @author ma
 */
public class ShoppingCart {

    private ArrayList<Order> cartList;

    public ShoppingCart() {
        this.cartList = new ArrayList<Order>();
    }

    public ShoppingCart(Order o) {
        this.cartList = new ArrayList<Order>();
        this.cartList.add(o);
    }

    public int addToCart(Order o) {
        int result = 0;
        try {
            this.cartList.add(o);
            result = 1;
        } catch (Exception e) {
        }
        return result;
    }

    public ArrayList<Order> getOrders() {
        return this.cartList;
    }
}
