/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author ma
 */
public class Order {
    private int size;
    private String[] toppings;
    public Order(){}
    public Order(int size,String[] toppings){
        this.size = size;
        this.toppings = toppings;
    }
    
    public int getSize(){
        return this.size;
    }
    
    public String[] getToppings(){
        return this.toppings;
    }
}
