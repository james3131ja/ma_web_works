/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import Models.PizzaOrderForm;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 *
 * @author ma
 */
public class PizzaProcessContextListener implements ServletContextListener{

   
    
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext ctx = sce.getServletContext();
        String[] sizes = ctx.getInitParameter("size").toString().split("\\ ");
        String[] toppings = ctx.getInitParameter("toppings").toString().split("\\ ");
        //System.out.print(sizes[1]);
        PizzaOrderForm order = new PizzaOrderForm(sizes,toppings);
        ctx.setAttribute("myapp.orderFormat", order);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        ServletContext ctx = sce.getServletContext();
        ctx.removeAttribute("myapp.orderFormat");
    }
    
    
}
