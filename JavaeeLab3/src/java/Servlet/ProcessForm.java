/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Models.Order;
import Models.ShoppingCart;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ma
 */
@WebServlet(name = "ProcessForm", urlPatterns = {"/ProcessForm"})
public class ProcessForm extends HttpServlet {

    private File file = null;
    ServletContext ctx = null;
    String myName = null;
    String size = null;
    String[] toppings = null;
    String name = null;
    String cid = null;
    HttpSession session;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        try (PrintWriter out = response.getWriter()) {

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        size = request.getParameter("size");
        toppings = request.getParameterValues("Toppings");
        name = request.getParameter("customer_name");
        cid = request.getParameter("customer_id");

        request.setAttribute("size", size);
        request.setAttribute("Toppings", toppings);
        request.setAttribute("customer_name", name);
        request.setAttribute("customer_id", cid);

        boolean error = false;

        try {
            PrintWriter out = response.getWriter();
            //start printing errors over a form
            out.println("<html><head><title>Pizza Order Ma LUO</title></head><body>");
            if (!(cid == null || name == null)) {

                if (cid.length() == 0) {
                    out.println("<p><font color=red >" + "Customer Id required !!" + "</font></p>");
                    error = true;
                    ctx.log("Customer Id required !!");
                }
                if (!cid.matches("[0-9]{6}")) {
                    out.println("<p><font color=red >" + "Customer Id doesn't match 6 digits !!" + "</font></p>");
                    ctx.log("Customer Id doesn't match 6 digits !!");
                    error = true;
                }
                if (name.length() == 0) {
                    out.println("<p><font color=red>" + "Customer name required !!" + "</font></p>");
                    ctx.log("Customer name required !!");
                    error = true;
                }
                if (size == null) {
                    out.println("<p><font color=red >" + "Size required !!" + "</font></p>");
                    ctx.log("Size required !!");
                    error = true;
                }
                if (toppings == null) {
                    out.println("<p><font color=red >" + "Toppings required !!" + "</font></p>");
                    ctx.log("Toppings required !!");
                    error = true;
                }
            }

            if (error) {
                RequestDispatcher rd
                        = ctx.getRequestDispatcher("/form.jsp");
                rd.include(request, response);
                out.println("</body></html>");
            } else {
                logData(makeLogrow());
                confirmOrder(request, response);
                setShoppingCart(request);
            }
            out.close();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    private boolean isNewCustomer(HttpServletRequest request) {
        boolean result = true;
        session = request.getSession();
        try {
            if (cid.equals((String) session.getAttribute("Customer_Id"))) {
                result = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //session.invalidate();
        return result;
    }

    private void setShoppingCart(HttpServletRequest request) {
        Order o = new Order(Integer.parseInt(size), toppings);
        try{
        if (isNewCustomer(request)) {
            session = request.getSession();
            session.setAttribute("ObjCart", new ShoppingCart(o));
            session.setAttribute("Customer_Name", name);
            session.setAttribute("Customer_Id", cid);
        } else {
            session = request.getSession();
            ShoppingCart cart = (ShoppingCart) session.getAttribute("ObjCart");
            cart.addToCart(o);
            session.setAttribute("ObjCart", cart);
        }
        }catch(Exception e){}
    }

    //after the validatoin process

    private void logData(String row) {
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(file.getAbsoluteFile(), true));
            bw.write(row);
            bw.close();
        } catch (Exception e) {

        }
    }

    private String makeLogrow() {
        String selection = "";
        for (String item : toppings) {
            selection += item + " ";
        }
        DateFormat dateFormatDate = new SimpleDateFormat("yyyy/MM/dd");
        DateFormat dateFormatTime = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date();
        String outDate = dateFormatDate.format(date);
        String outTime = dateFormatTime.format(date);
        return cid + "\t" + name + "\t" + size + "\t" + selection + "\t" + outDate + "\t" + outTime + "\n";
    }

    private void confirmOrder(HttpServletRequest request,
            HttpServletResponse res) {
        String size = request.getParameter("size");
        String[] toppings = request.getParameterValues("Toppings");
        String name = request.getParameter("customer_name");
        String cid = request.getParameter("customer_id");
        try {
            PrintWriter out = res.getWriter();
            res.setContentType("text/html");
            out.println("<html><head><title>Order Confirmation</title></head><body>");
            out.println("Hello " + name + " !! Your order for a ");
            out.println(size + " inch with ");
            for (int i = 0; i < toppings.length; i++) {
                out.println(toppings[i] + " ");
            }
            out.println("is on the way!<hr>");
            out.println("Click <a href='./form.jsp'>here</a> to order another");
            out.println("</body></html>");
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    //fileLocation
    @Override
    public void init(ServletConfig cfg) {
        String name = cfg.getInitParameter("fileLocation");
        ctx = cfg.getServletContext();
        myName = cfg.getServletName();

        try {
            file = new File(name);
            if (!file.exists()) {
                file.createNewFile();
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void destroy() {
        file = null;
    }
}
