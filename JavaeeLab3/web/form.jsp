<%-- 
    Document   : form.jsp
    Created on : 18-Sep-2014, 6:05:19 PM
    Author     : ma
--%>
<%@page import="Models.PizzaOrderForm"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
   String cid =  request.getParameter("customer_id");
   String name = request.getParameter("customer_name");
   String[] toppings = request.getParameterValues("Toppings");
   String  size = request.getParameter("size");
   PizzaOrderForm obj = (PizzaOrderForm)getServletContext().getAttribute("myapp.orderFormat");
   String[] toppingOffered = obj.getToppingGroup();

 %>
<%! 
private boolean checkTopping(String value,String[] toppings){
if(toppings !=  null){
for(String item : toppings){
if(item.equals(value)){return true;}
}}
return false;
}
%>
 
<fieldset>
    <legend>Pizza Order Form</legend>
    <!--remember to use . here!!!!-->
    <form id="orderForm" action="ProcessForm.html" method="post">
        <label for="cid">Customer ID : </label><input type="text" name="customer_id" id="cid" value=<%=(cid==null)?"":cid%>>
        <label for="cname">Customer Name : </label><input type="text" name="customer_name" id="cname" value=<%=(name==null)?"":name%>><br/>
        Size:
<%
        for(String item : obj.getSizeGroup()){
%>        
        <input type="radio" name="size" value=<%=item%> <%=(item.equals(size))?"checked":""%>> <%=item%>
<%               
        }
%>
        <br/>
        Toppings<select name="Toppings" multiple>
<%
for(String item : toppingOffered){
%>
           <option <%=(checkTopping(item,toppings))?"selected":""%> ><%=item%></option>
<%
}
%>
        </select>
        <br/>
        <input type="submit" name="btn_submit" value="Order">
        <input type="button" name="btn_submit" value="Show Cart" onclick="showCart()">
    </form>   
</fieldset>
<script src="./js/jquery-1.11.1.min.js"></script>
<script src="./js/Cart.js"></script>

<div id="cartList">
    
</div>