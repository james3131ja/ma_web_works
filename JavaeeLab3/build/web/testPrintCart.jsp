<%-- 
    Document   : testPrintCart
    Created on : 9-Oct-2014, 11:00:07 AM
    Author     : ma
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="Models.Order"%>
<%@page import="Models.ShoppingCart"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
ShoppingCart cart = (ShoppingCart) session.getAttribute("ObjCart");
String cid = (String)session.getAttribute("Customer_Id");
String name = (String)session.getAttribute("Customer_Name");
%>

<%!
public String splitToppings(String[] toppings){
    String result = "";
    for(String i : toppings){
    result+= i+" ";
    }
    return result;
}
%>
        <table>
            <thead>
            <th>Customer Id</th>
            <th>Name</th>
            <th>Toppings</th>
            <th>Size</th>
            </thead>
            <%
            for(Order o : cart.getOrders()){
            %>    
            <tr>
                <td>
                <%=cid%>
                </td>
                <td>
                <%=name%>
                </td>
                <td>
                <%=splitToppings(o.getToppings())%>    
                </td>
                <td>
                <%=o.getSize()%>
                </td>
            </tr>
            <%
            }
            %>
        </table>