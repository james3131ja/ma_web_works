<%@ page contentType="text/html; charset=gb2312" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "com.se.roombooking.db.dao.impl.Applicationdao_impl" %>
<%@ page import = "java.util.LinkedList" %>
<%@ page import = "com.se.roombooking.vo.Application" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title>RoomInformation</title>

<style type="text/css">
table{
	text-align:center
}
.search table#searchbar{
	position:relative;left:250px; border:1px solid black;}
.search{
	background-image:url('../images/topbar.png')
}
.table-main
  {
  font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
  width:100%;
  border-collapse:collapse;
  }

.table-main td, .table-main th 
  {
  font-size:1em;
  border:1px solid #98bf21;
  padding:3px 7px 2px 7px;
  }

.table-main th 
  {
  font-size:1.1em;
  text-align:left;
  padding-top:5px;
  padding-bottom:4px;
  background-color:#A7C942;
  color:#ffffff;
  }

.table-main tr.alt td 
  {
  color:#000000;
  background-color:#EAF2D3;
  }
</style>
</head>

<body>
<div id="web">
  <div class="website">
    <div class="top">此处显示  id "web" 的内容</div>
	
    <div class="content" style=" width: auto;height:350px;background-image:url('<%=request.getContextPath()%>/images/background.jpg');">
      	<div class="Logout" style=" text-align:right;font-size:18px">UserName            <a href="main.jsp">Logout</a></div>
		<div class="nothing" style=" text-align:right;font-size:20px; font-weight:bold"></div>
		<div class="PageName" style=" text-align:right;font-size:20px; font-weight:bold">History of Application&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
		<div class="condition">
			<div align="center">
			<span id="RoomNo">
			  RoomNo. :
			  <input name="No" type="text" style="width:50px" value="all"/>
		    </span>
			
			
			    <span id="time">
			      Time:
			      From
			      <select id="startTime" onchange="getSelected(this)">
			        <option value="0"selected="selected">all</option>
			        <option value="6">6;00</option>
			        <option value="7">7;00</option>
			        <option value="8">8;00</option>
			        <option value="9">9;00</option>
			        <option value="10">10;00</option>
			        <option value="11">11;00</option>
			        <option value="12">12;00</option>
			        <option value="13">13;00</option>
			        <option value="14">14;00</option>
			        <option value="15">15;00</option>
			        <option value="16">16;00</option>
			        <option value="17">17;00</option>
			        <option value="18">18;00</option>
			        <option value="19">19;00</option>
			        <option value="20">20;00</option>
			        <option value="21">21;00</option>
			        <option value="22">22;00</option>
		        </select>
			      
			      To
			      <select id="startTime" onchange="getSelected(this)">
			        <option value="0" selected="selected">all</option>
			        <option value="6">6;00</option>
			        <option value="7">7;00</option>
			        <option value="8">8;00</option>
			        <option value="9">9;00</option>
			        <option value="10">10;00</option>
			        <option value="11">11;00</option>
			        <option value="12">12;00</option>
			        <option value="13">13;00</option>
			        <option value="14">14;00</option>
			        <option value="15">15;00</option>
			        <option value="16">16;00</option>
			        <option value="17">17;00</option>
			        <option value="18">18;00</option>
			        <option value="19">19;00</option>
			        <option value="20">20;00</option>
			        <option value="21">21;00</option>
			        <option value="22">22;00</option>
                </select>
	          </span>
			  <span id="Capacity">
		        Capacity:
		        <input name="Capacity" type="text" style="width:30px" value="all"/>
		      </span>
			  <span id="facilities">
			Facilities:
			  <input name="Facilities" type="checkbox" value="computer" />computer
			  <input name="Facilities" type="checkbox" value="projecter" />projecter
			  <input name="Facilities" type="checkbox" value="microphone" />microphone
			  <input name="Facilities" type="checkbox" value="blackboard" />
			  blackboard
			
		    </span>
			    
			    
	      </div>
		</div>
		<div class="secondLine" align="center">
		<span id="date">
			  Date :
			  From
			  <input name="DateFrom" type="text" style="width:80px" maxlength="10"/>
			  To
			  <input name="DateTo" type="text" style="width:80px" maxlength="10"/>
			  (eg.YYYY-MM-DD)
			  </span>&nbsp;&nbsp;&nbsp;<span id="week">Week:
			<input type="checkbox" name="DaysOfWeek" value="Mon" />Mon
			<input type="checkbox" name="DaysOfWeek" value="Tue" />Tue
			<input type="checkbox" name="DaysOfWeek" value="Wed" />Wed
			<input type="checkbox" name="DaysOfWeek" value="Thu" />Thu
			<input type="checkbox" name="DaysOfWeek" value="Fri" />Fri
			<input type="checkbox" name="DaysOfWeek" value="Sta" />Sta
			<input type="checkbox" name="DaysOfWeek" value="Sun" />Sun			</span>
			&nbsp;&nbsp;&nbsp;&nbsp;
		
			<span id="SearchButton">
			      <input name="Search" type="button" value="Search" />
	        </span>
		</div>
		<div class="table">
		<table width="90%" border="1" class = "table-main">
		<tr>
		<th scope="col">Application Id</th>
    		<th scope="col">Room No</th>
    		<th scope="col">Applicant Name</th>
    		<th scope="col">Date From</th>
    		<th scope="col">Date To</th>
    		<th scope="col">Time From</th>
    		<th scope="col">Time To</th>
    		<th scope="col">Days of Week</th>
    		<th scope="col">State</th>
    		<th scope="col">Capacity</th>
    		<th scope="col">Facilities</th>
    		<th scope="col">Hours Count</th>
		</tr>
		<%
			//留作别处用
			if(Integer.parseInt(session.getAttribute("priority").toString()) < 1 | session.getAttribute("user_name")== null){
				response.sendRedirect("FunctionList.jsp");
			}else{
			int counter = 0;
			Applicationdao_impl adi = new Applicationdao_impl();
			LinkedList<Application> applicationlist = adi.get_application_byname(session.getAttribute("user_name").toString());
			counter = applicationlist.size()-1;
			//System.out.print(applicationlist.size());
			String[] str= null; 
			while(counter>=0){
			%>
			 <!-- 这里onlick 传出对应链表的node的值，在另个一个页面中用jsp得到链表并print出来。点击confirm添加进入数据库 -->
	
			<tr>
    			<td><%=applicationlist.get(counter).get_applicant_id() %> </td>
   	 			<td><%=applicationlist.get(counter).get_room_no() %></td>
   	 			<td><%=applicationlist.get(counter).get_applicant_name() %></td>
   	 			<td><%=applicationlist.get(counter).get_date_beg() %></td>
   	 			<td><%=applicationlist.get(counter).get_date_end() %></td>
   	 			<td><%=applicationlist.get(counter).get_time_beg() %></td>
   	 			<td><%=applicationlist.get(counter).get_time_end() %></td>
   	 			<td><%=applicationlist.get(counter).get_days_of_week() %></td>
   	 			<td><%=applicationlist.get(counter).get_room_state() %></td>
   	 			<td><%=applicationlist.get(counter).get_capacity() %></td>
   	 			<td><%=applicationlist.get(counter).get_facilities() %></td>
   	 			<td><%=applicationlist.get(counter).get_hours_count() %></td>
 		    </tr>
		   <%counter--;}
		   }%>
		</table>	
		</div>
	</div>
    <div id="footer">此处显示  id "footer" 的内容</div>
  </div>
</div>

</body>
</html>
