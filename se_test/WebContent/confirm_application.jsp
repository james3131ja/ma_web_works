<%@ page contentType="text/html; charset=gb2312" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "com.se.roombooking.db.dao.impl.Applicationdao_impl" %>
<%@ page import = "com.se.roombooking.db.dao.impl.Roomdao_impl" %>
<%@ page import = "java.util.LinkedList" %>
<%@ page import = "com.se.roombooking.vo.Application" %>
<%@ page import = "com.se.roombooking.vo.Room" %>
<%@ page import = "java.util.*" %>
<%@ page import = "java.util.Date" %>
<%@ page import = "java.text.SimpleDateFormat" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@page import="org.apache.jasper.tagplugins.jstl.core.Redirect"%><html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title>Application</title>

<style type="text/css">
table{
	text-align:center
}
.search table#searchbar{
	position:relative;left:250px; border:1px solid black;}
.search{
	background-image:url('../images/topbar.png')
}
.table-main
  {
  font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
  width:100%;
  border-collapse:collapse;
  }

.table-main td, .table-main th 
  {
  font-size:1em;
  border:1px solid #98bf21;
  padding:3px 7px 2px 7px;
  }

.table-main th 
  {
  font-size:1.1em;
  text-align:left;
  padding-top:5px;
  padding-bottom:4px;
  background-color:#A7C942;
  color:#ffffff;
  }

.table-main tr.alt td 
  {
  color:#000000;
  background-color:#EAF2D3;
  }
</style>
</head>

<body>

<div class ="table">	
		<table width="90%" border="1" class = "table-main">
		
<%
		if(session.getAttribute("user_name")==null){
		response.sendRedirect("main.jsp");
		}
			else{
			int counter = 0;
			Roomdao_impl rdi = new Roomdao_impl();
			LinkedList<Room> roomlist = rdi.get_room_list_roomno(request.getParameter("room_no"));
			Application application_form = new Application();
			application_form.set_room_no(request.getParameter("room_no"));
			String applcant_name = 	session.getAttribute("user_name").toString();
			application_form.set_applicant_name(applcant_name);
			counter = roomlist.size()-1;
			application_form.set_date_beg(roomlist.get(counter).get_date_beg());
			application_form.set_date_end(roomlist.get(counter).get_date_end());
			application_form.set_time_beg(roomlist.get(counter).get_time_beg());
			application_form.set_time_end(roomlist.get(counter).get_time_end());
			application_form.set_days_of_week(roomlist.get(counter).get_days_of_week());
			application_form.set_capacity(roomlist.get(counter).get_capacity());
			application_form.set_facilities(roomlist.get(counter).get_facilities());
			application_form.set_hours_count(roomlist.get(counter).get_hours_count());
			
			SimpleDateFormat dataTran = new SimpleDateFormat("yyyy-MM-dd");
			
			java.util.Date dBeg = (java.util.Date) roomlist.get(counter).get_date_beg();
			String dateBeg = dataTran.format(dBeg);
			java.util.Date dEnd = (java.util.Date) roomlist.get(counter).get_date_end();
			String dateEnd = dataTran.format(dEnd);
			session.setAttribute("applicant_form",application_form);
			//System.out.print(applicationlist.size());
			while(counter>=0){
			%>
			 <!-- 这里onlick 传出对应链表的node的值，在另个一个页面中用jsp得到链表并print出来。点击confirm添加进入数据库 -->
			<tr>
    			<td>Room No </td>
    			<td><%=application_form.get_room_no() %> </td>
 		    </tr>
 		    
 		    <tr>
    			<td>Applicant Name</td>
    			<td><%=application_form.get_applicant_name() %></td>
 		   </tr>
 		
 		   <tr>
    			<td>Date From</td>
    			<td><%=dBeg%> </td>
 		   </tr>
 		   
 		   <tr>
    			<td>Date To</td>
    			<td><%=dEnd %> </td>
 		   </tr>
 		   
 		    <tr>
    			<td>Time From</td>
    			<td><%=application_form.get_time_beg() %> </td>
 		   </tr>
 		   
 		    <tr>
    			<td>Time To</td>
    			<td><%=application_form.get_time_end() %> </td>
 		   </tr>
 		   
 		    <tr>
    			<td>Days of Week</td>
    			<td><%=application_form.get_days_of_week() %></td>
 		   </tr>
 		   
 		    <tr>
    			<td>Capacity</td>
    			<td><%=application_form.get_capacity() %></td>
 		   </tr>
 		   
 		   <tr>
    			<td>Facilities</td>
    			<td><%=application_form.get_facilities() %></td>
 		   </tr>
 		   
 		   <tr>
    			<td>hours_count</td>
    			<td><%=application_form.get_hours_count() %></td>
 		   </tr>
		   <%counter--;}
			}
		   %>
		    <tr>
    			<td><a href="send_application_form">apply</a></td>
 		   </tr>
  		</table>
		</div>

</body>
</html>
