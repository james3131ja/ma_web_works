package com.se.roombooking.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.se.roombooking.db.dao.impl.Applicationdao_impl;
import com.se.roombooking.vo.Application;

/**
 * Servlet implementation class manage_application
 * get_application_list()
 * choose accept or reject
 * update database
 */
public class manage_application extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public manage_application() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(request.getParameter("state") == null | request.getSession().getAttribute("applicationId").toString() == null)
		{
		response.sendRedirect("FunctionList.jsp");
		}else{
			Applicationdao_impl adi = new Applicationdao_impl();
			try {
				adi.modify_application_state(Integer.parseInt(request.getSession().getAttribute("applicationId").toString()),
						Integer.parseInt(request.getParameter("state").toString()));
				response.sendRedirect("FunctionList.jsp");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
