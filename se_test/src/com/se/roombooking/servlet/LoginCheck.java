package com.se.roombooking.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.se.roombooking.db.dao.impl.Userdao_impl;
import com.se.roombooking.vo.User;


/**
 * Servlet implementation class LoginCheck
 * 
 * input and action check login 1.if login 2.if not register 3.login successful
 * direct to homepage in case 2, otherwise go to error pages
 */

public class LoginCheck extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginCheck() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Userdao_impl udi = new Userdao_impl();
		User user = new User();
		user.set_username(request.getParameter("userid"));
		user.set_password(request.getParameter("password"));
		try {
			if (udi.validate_database_info(user.get_username())) {
				if(udi.validate_user(user.get_username(),user.get_password())){
				HttpSession session = request.getSession(true);
				session.setAttribute("user_name", request.getParameter("userid"));
				session.setAttribute("password", request.getParameter("password"));
				//设置操作权限
				int priotiry  = udi.get_user_byname(user.get_username()).get_property();
				session.setAttribute("priority",priotiry);
				System.out.println("test_user_dao_impl:validate user in database!");
				response.sendRedirect("FunctionList.jsp");
				}else{
				//跳转至error page再回到主页
				//error page: 显示用户登录失败	
				}
			} else {
				System.out.println("用户不存在");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request,response);
	}

}
