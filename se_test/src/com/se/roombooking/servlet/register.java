package com.se.roombooking.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.se.roombooking.db.dao.impl.Userdao_impl;
import com.se.roombooking.vo.User;

/**
 * Servlet implementation class register
 */
public class register extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public register() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Userdao_impl udi = new Userdao_impl();
		User user = new User();
		user.set_username(request.getParameter("userid"));
		user.set_password(request.getParameter("password"));
		System.out.println(user.get_username()+user.get_password()+"\n");
		
		if(user.get_username() == ""  | user.get_password() == ""){
			System.out.println("complete form please");
			response.sendRedirect("main.jsp");
		}
		else{
			try {
				if (udi.validate_database_info(user.get_username())) {
					System.out.println("用户已经存在");
					response.sendRedirect("main.jsp");
				} else {
					user.set_property(1);
					udi.add_user(user);
					response.sendRedirect("FunctionList.jsp");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}	
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request,response);
	}

}
