package com.se.roombooking.db.dao;

import java.sql.Date;
import java.sql.Time;
import java.util.LinkedList;

import com.se.roombooking.vo.Room;

public interface Roomdao {
//view available room list 
public boolean change_one_room_info(String room_no) throws Exception;

//user get room info
public LinkedList<Room> get_available_room_bydatetime(Date dateBeg, Date dateEnd,
		Time timeBeg, Time timeEnd) throws Exception;

public  LinkedList<Room> get_available_room_withcapacity(Date dateBeg, Date dateEnd,
		Time timeBeg, Time timeEnd,int capacity) throws Exception;

public LinkedList<Room> get_available_room_withfacilities(Date dateBeg, Date dateEnd,
		Time timeBeg, Time timeEnd,String facilities) throws Exception;

public LinkedList<Room> get_available_room_withcapfacilities(Date dateBeg, Date dateEnd,
		Time timeBeg, Time timeEnd,int capacity,String facilities) throws Exception;

public LinkedList<Room> get_room_list() throws Exception;	


public LinkedList<Room> get_room_list_roomno(String room_no) throws Exception;	


}
