package com.se.roombooking.db.dao;

import java.sql.ResultSet;
import java.util.LinkedList;
import com.se.roombooking.vo.User;

public interface Userdao {
//regiser
public boolean add_user(User user) throws Exception;

//all
public boolean validate_database_info(String check) throws Exception;

//admin user
public boolean delete_user(String user_name) throws Exception;

//all
public boolean validate_user(String user_name,String password) throws Exception;

//admin use
public LinkedList<User> get_user_list() throws Exception;


public User get_user_byname(String user_name) throws Exception;
}
