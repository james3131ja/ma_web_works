package com.se.roombooking.db.dao;

import java.util.LinkedList;

import com.se.roombooking.vo.Application;

public interface Applicationdao {
//add application
public boolean add_application(Application application) throws Exception;

//delete application
public boolean delete_application(int application_id) throws Exception;

//get application list
public LinkedList<Application> get_application_list() throws Exception;

//get application list with state
public LinkedList<Application> get_application_list_withstate(int state) throws Exception;

public boolean modify_application_state(int application_id,int state) throws Exception;

public LinkedList<Application> get_application_byid(int application_id) throws Exception;

public LinkedList<Application> get_application_byname(String user_name) throws Exception;
}
