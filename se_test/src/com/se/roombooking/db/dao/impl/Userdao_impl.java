package com.se.roombooking.db.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;

import com.se.roombooking.db.db_conn;
import com.se.roombooking.db.dao.Userdao;
import com.se.roombooking.vo.User;


public class Userdao_impl implements  Userdao{

	@Override
	public boolean add_user(User user) throws Exception {
		//the user id is automatically increased
		db_conn dbc = new db_conn();
		Connection conn = dbc.getConnection();
		PreparedStatement pStatement = null;
		boolean isSuccess = false;
		String insert_user = "INSERT INTO `se_project`.`user` (`user_name`, `password`, `priority`) VALUES (?,?,?);";
		try{
			//set values to insert
			pStatement = conn.prepareStatement(insert_user);
			pStatement.setString(1, user.get_username());
			pStatement.setString(2, user.get_password());
			pStatement.setInt(3, user.get_property());
			
			// 执行预处理语句 用 validate 接受返回的 int 判断是否执行成功
			int validate = 0;
			validate = pStatement.executeUpdate();
			// 判断是否执行成功
			if (validate > 0) {
				isSuccess = true;
				System.out.print("Userdao_impl:  添加user成功" + "\n\n");
			}
			dbc.close();
			return isSuccess;
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return isSuccess;
	}

	@Override
	public boolean delete_user(String user_name) throws Exception {
		// TODO Auto-generated method stub
		db_conn dbc = new db_conn();
		Connection conn = dbc.getConnection();
		PreparedStatement pStatement = null;
		boolean isSuccess = false;
		String delete_user = "DELETE FROM `se_project`.`user` WHERE `user_name` = ?;";
		try{
			//set user_name to delete
			pStatement = conn.prepareStatement(delete_user);
			pStatement.setString(1,user_name);
			
			// 执行预处理语句 用 validate 接受返回的 int 判断是否执行成功
			int validate = 0;
			validate = pStatement.executeUpdate();
			// 判断是否执行成功
			if (validate > 0) {
				isSuccess = true;
				System.out.print("Userdao_impl:  删除user成功" + "\n\n");
			}
			dbc.close();
			return isSuccess;
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return isSuccess;
	}

	//get the user list
	@Override
	public LinkedList<User> get_user_list() throws Exception {
		// TODO Auto-generated method stub
		db_conn dbc = new db_conn();
		Connection conn = dbc.getConnection();
		PreparedStatement pStatement = null;
		LinkedList<User> userlist = new LinkedList<User>();
		ResultSet rs = null; 
		String select_all_user = "SELECT * FROM  `se_project`.`user`;";
		try{
			//set user_name to delete
			pStatement = conn.prepareStatement(select_all_user);
			try{
			rs = pStatement.executeQuery(select_all_user);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if (rs != null) {
				while (rs.next()) {
					User user = new User();
					user.set_username(rs.getString("user_name"));
					user.set_password(rs.getString("password"));
					user.set_property(rs.getInt("priority"));
					userlist.add(user);
				}
			}
			
			//if empty, then rs.next() is false
			if (!rs.next()){
				System.out.println("User list empty");
			}
			dbc.close();
			return userlist;
		}catch(Exception e){
			e.printStackTrace();
		}
		return userlist;
	}

	//check whehter the user is in database
	@Override
	public boolean validate_database_info(String check) throws Exception {
		db_conn dbc = new db_conn();
		Connection conn = dbc.getConnection();
		PreparedStatement pStatement = null;
		ResultSet rs = null; 
		boolean isBoolean = false;
		int count_record = 0;
		String search_one_user = "SELECT * FROM  `se_project`.`user` WHERE `user`.`user_name` = '"+check+"';";
		try{
			//set user_name to delete
			pStatement = conn.prepareStatement(search_one_user);
			try{
			rs = pStatement.executeQuery(search_one_user);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if (rs != null) {
				while (rs.next()) {
					count_record++;
				}
			}
					
			if (count_record>0) {
				if(count_record != 1){
					System.out.println("Userdao_impl: User duplicated");
					isBoolean = false;
				}
				else{
					isBoolean = true;;	
				}
			}else{
				System.out.println("Userdao_impl: no this user in database");
			}
			
			dbc.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return isBoolean;
	}

	//数据库只有一行，select后可以算到这一行，但是无法执行rs操作
	@Override
	public boolean validate_user(String user_name, String password) throws Exception {
		// TODO Auto-generated method stub
		//do not let user = null, or you will get in trouble
		User user = new User();
		db_conn dbc = new db_conn();
		Connection conn = dbc.getConnection();
		PreparedStatement pStatement = null;
		ResultSet rs = null; 
		boolean isSuccess = false;
		String select_one_user = "SELECT * FROM  `se_project`.`user`" +
				" WHERE `user`.`user_name` = '"+user_name+"' " +
				"AND `user`.`password` = '"+password+"';";
		
			//set user_name to delete
			pStatement = conn.prepareStatement(select_one_user);
			try{
			rs = pStatement.executeQuery(select_one_user);
			
			if (rs.next()) {
				user.set_username(rs.getString("user_name"));
				user.set_password(rs.getString("password"));
				System.out.print("Userdao_impl:"+user.get_username()+"\n");
				isSuccess = true;
			}
			dbc.close();
			}catch(Exception e){
				e.printStackTrace();
			}
		return isSuccess;
	}

	//use user_name as keyword to search user
	public User get_user_byname(String user_name) throws Exception{
		User user = null;
		db_conn dbc = new db_conn();
		Connection conn = dbc.getConnection();
		PreparedStatement pStatement = null;
		ResultSet rs = null; 
		String select_one_user = "SELECT * FROM  `se_project`.`user`" +
		" WHERE `user`.`user_name` = '"+user_name+"';" ;
		
		//set user_name to delete
		pStatement = conn.prepareStatement(select_one_user);
		try{
		rs = pStatement.executeQuery(select_one_user);
		
		if (rs.next()) {
			user = new User();
			user.set_username(rs.getString("user_name"));
			user.set_password(rs.getString("password"));
			user.set_property(rs.getInt("priority"));
			System.out.print("Userdao_impl:"+user.get_username()+"\n");
		}
		dbc.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return user;
	}

}
