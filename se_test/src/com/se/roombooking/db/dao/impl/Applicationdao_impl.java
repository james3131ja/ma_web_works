package com.se.roombooking.db.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;

import com.se.roombooking.db.db_conn;
import com.se.roombooking.db.dao.Applicationdao;
import com.se.roombooking.vo.Application;

public class Applicationdao_impl implements Applicationdao{

	@Override
	public boolean add_application(Application application) throws Exception {
		// TODO Auto-generated method stub
		db_conn dbc = new db_conn();
		Connection conn = dbc.getConnection();
		PreparedStatement pStatement = null;
		boolean isSuccess = false;
		
		String insert_user = "INSERT INTO `se_project`.`application` (`room_no`,`applicant_name`, `dateBeg`, `dateEnd`, `daysOfWeek`, `timeBeg`, `timeEnd`, `state`, `capacity`, `facilities`) VALUES (?,?, ?, ?, ?, ?, ?, ?,?,?);";
		try{
			//set values to insert
			pStatement = conn.prepareStatement(insert_user);
			
			pStatement.setString(1, application.get_room_no());
			pStatement.setString(2, application.get_applicant_name());
			pStatement.setDate(3, application.get_date_beg());
			pStatement.setDate(4, application.get_date_end());
			pStatement.setString(5, application.get_days_of_week());			
			pStatement.setTime(6, application.get_time_beg());
			pStatement.setTime(7, application.get_time_end());
			pStatement.setInt(8, application.get_room_state());
			pStatement.setInt(9, application.get_capacity());
			pStatement.setString(10, application.get_facilities());
			
			// 执行预处理语句 用 validate 接受返回的 int 判断是否执行成功
			int validate = 0;
			validate = pStatement.executeUpdate();
			// 判断是否执行成功
			if (validate > 0) {
				isSuccess = true;
				System.out.print("Applicationdao_impl:  添加application成功" + "\n\n");
			}
			dbc.close();
			return isSuccess;
		}catch(Exception e){
			e.printStackTrace();
		}
		return isSuccess;
	}

	public boolean delete_application(int application_id) throws Exception {
		// TODO Auto-generated method stub
		db_conn dbc = new db_conn();
		Connection conn = dbc.getConnection();
		PreparedStatement pStatement = null;
		boolean isSuccess = false;
		String delete_application = "DELETE FROM `se_project`.`application` WHERE `applicationId` = ?;";
		try{
			//set user_name to delete
			pStatement = conn.prepareStatement(delete_application);
			pStatement.setInt(1,application_id);
			
			// 执行预处理语句 用 validate 接受返回的 int 判断是否执行成功
			int validate = 0;
			validate = pStatement.executeUpdate();
			// 判断是否执行成功
			if (validate > 0) {
				isSuccess = true;
				System.out.print("Applicationdao_impl:  删除application成功" + "\n\n");
			}
			dbc.close();
			return isSuccess;
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return isSuccess;
	}

	//applications with state
	//未批准的申请
	@SuppressWarnings("null")
	public LinkedList<Application> get_application_list_withstate(int state) throws Exception{
		db_conn dbc = new db_conn();
		Connection conn = dbc.getConnection();
		PreparedStatement pStatement = null;
		LinkedList<Application> applicationlist = new LinkedList<Application>();
		ResultSet rs = null; 
		
		String select_all_application = "SELECT * FROM  `se_project`.`application` WHERE `application`.`state` = '"+state+"';";
		try{
			//set user_name to delete
			pStatement = conn.prepareStatement(select_all_application);
			try{
			rs = pStatement.executeQuery(select_all_application);
			}catch(Exception e){
				e.printStackTrace();
			}
	
			if (rs != null) {
				while (rs.next()) {
					Application application = new Application();
					application.set_applicant_id(rs.getInt("applicationId"));
					application.set_room_no(rs.getString("room_no"));
					application.set_applicant_name(rs.getString("applicant_name"));
					application.set_date_beg(rs.getDate("dateBeg"));
					application.set_date_end(rs.getDate("dateEnd"));
					application.set_days_of_week(rs.getString("daysOfWeek"));			
					application.set_time_beg(rs.getTime("timeBeg"));
					application.set_time_end(rs.getTime("timeEnd"));
					application.set_room_state(rs.getInt("state"));
					application.set_capacity(rs.getInt("capacity"));
					application.set_facilities(rs.getString("facilities"));
					application.set_hours_count(rs.getInt("hours_count"));
					applicationlist.add(application);
				}
			}
			//if empty, then rs.next() is false
			else{
			if (!rs.next()){
				System.out.println("User list empty");
			}
			}
			//System.out.println("test applicationdao_impl applicationlist state"+applicationlist.size());
			dbc.close();
			return applicationlist;
		}catch(Exception e){
			e.printStackTrace();
		}
		return applicationlist;
		
	}
	
	//all applications
	@SuppressWarnings("null")
	@Override
	public LinkedList<Application> get_application_list() throws Exception {
		// TODO Auto-generated method stub
		db_conn dbc = new db_conn();
		Connection conn = dbc.getConnection();
		PreparedStatement pStatement = null;
		LinkedList<Application> applicationlist = new LinkedList<Application>();
		ResultSet rs = null; 
		String select_all_application = "SELECT * FROM  `se_project`.`application`;";
		try{
			//set user_name to delete
			pStatement = conn.prepareStatement(select_all_application);
			try{
			rs = pStatement.executeQuery(select_all_application);
			}catch(Exception e){
				e.printStackTrace();
			}
	
			if (rs != null) {
				while (rs.next()) {
					Application application = new Application();
					application.set_applicant_id(rs.getInt("applicationId"));
					application.set_room_no(rs.getString("room_no"));
					application.set_applicant_name(rs.getString("applicant_name"));
					application.set_date_beg(rs.getDate("dateBeg"));
					application.set_date_end(rs.getDate("dateEnd"));
					application.set_days_of_week(rs.getString("daysOfWeek"));			
					application.set_time_beg(rs.getTime("timeBeg"));
					application.set_time_end(rs.getTime("timeEnd"));
					application.set_room_state(rs.getInt("state"));
					application.set_capacity(rs.getInt("capacity"));
					application.set_facilities(rs.getString("facilities"));
					application.set_hours_count(rs.getInt("hours_count"));
					applicationlist.add(application);
				}
			}
			//System.out.println(applicationlist.size());
			//if empty, then rs.next() is false
			else{
			if (!rs.next()){
				System.out.println("User list empty");
			}
			}
			dbc.close();
			return applicationlist;
		}catch(Exception e){
			e.printStackTrace();
		}
		return applicationlist;
	}
	
	public boolean modify_application_state(int application_id,int state) throws Exception{
		db_conn dbc = new db_conn();
		Connection conn = dbc.getConnection();
		PreparedStatement pStatement = null;
		boolean isSuccess = false;
		String delete_application = "UPDATE `se_project`.`application` SET `state` = '"+state+"' WHERE `application`.`applicationId` = '"+application_id+"';";
		try{
			//set user_name to delete
			pStatement = conn.prepareStatement(delete_application);
			
			// 执行预处理语句 用 validate 接受返回的 int 判断是否执行成功
			int validate = 0;
			validate = pStatement.executeUpdate();
			// 判断是否执行成功
			if (validate > 0) {
				isSuccess = true;
				System.out.print("Applicationdao_impl:  删除application成功" + "\n\n");
			}
			dbc.close();
			return isSuccess;
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return isSuccess;
		
	}

	@SuppressWarnings("null")
	@Override
	public LinkedList<Application> get_application_byid(int applicationId) throws Exception {
		// TODO Auto-generated method stub
		db_conn dbc = new db_conn();
		Connection conn = dbc.getConnection();
		PreparedStatement pStatement = null;
		LinkedList<Application> applicationlist = new LinkedList<Application>();
		ResultSet rs = null; 
		
		String select_all_application = "SELECT * FROM  `se_project`.`application` WHERE `application`.`applicationId` = '"+applicationId+"';";
		try{
			//set user_name to delete
			pStatement = conn.prepareStatement(select_all_application);
			try{
			rs = pStatement.executeQuery(select_all_application);
			}catch(Exception e){
				e.printStackTrace();
			}
	
			if (rs != null) {
				while (rs.next()) {
					Application application = new Application();
					application.set_applicant_id(rs.getInt("applicationId"));
					application.set_room_no(rs.getString("room_no"));
					application.set_applicant_name(rs.getString("applicant_name"));
					application.set_date_beg(rs.getDate("dateBeg"));
					application.set_date_end(rs.getDate("dateEnd"));
					application.set_days_of_week(rs.getString("daysOfWeek"));			
					application.set_time_beg(rs.getTime("timeBeg"));
					application.set_time_end(rs.getTime("timeEnd"));
					application.set_room_state(rs.getInt("state"));
					application.set_capacity(rs.getInt("capacity"));
					application.set_facilities(rs.getString("facilities"));
					application.set_hours_count(rs.getInt("hours_count"));
					applicationlist.add(application);
				}
			}
			//if empty, then rs.next() is false
			else{
			if (!rs.next()){
				System.out.println("User list empty");
			}
			}
			//System.out.println("test applicationdao_impl applicationlist state"+applicationlist.size());
			dbc.close();
			return applicationlist;
		}catch(Exception e){
			e.printStackTrace();
		}
		return applicationlist;
	}
	
	public LinkedList<Application> get_application_byname(String user_name) throws Exception{
		db_conn dbc = new db_conn();
		Connection conn = dbc.getConnection();
		PreparedStatement pStatement = null;
		LinkedList<Application> applicationlist = new LinkedList<Application>();
		ResultSet rs = null; 
		
		String select_all_application = "SELECT * FROM  `se_project`.`application` WHERE `application`.`applicant_name` = '"+user_name+"';";
		try{
			//set user_name to delete
			pStatement = conn.prepareStatement(select_all_application);
			try{
			rs = pStatement.executeQuery(select_all_application);
			}catch(Exception e){
				e.printStackTrace();
			}
	
			if (rs != null) {
				while (rs.next()) {
					Application application = new Application();
					application.set_applicant_id(rs.getInt("applicationId"));
					application.set_room_no(rs.getString("room_no"));
					application.set_applicant_name(rs.getString("applicant_name"));
					application.set_date_beg(rs.getDate("dateBeg"));
					application.set_date_end(rs.getDate("dateEnd"));
					application.set_days_of_week(rs.getString("daysOfWeek"));			
					application.set_time_beg(rs.getTime("timeBeg"));
					application.set_time_end(rs.getTime("timeEnd"));
					application.set_room_state(rs.getInt("state"));
					application.set_capacity(rs.getInt("capacity"));
					application.set_facilities(rs.getString("facilities"));
					application.set_hours_count(rs.getInt("hours_count"));
					applicationlist.add(application);
				}
			}
			//if empty, then rs.next() is false
			else{
			if (!rs.next()){
				System.out.println("User list empty");
			}
			}
			//System.out.println("test applicationdao_impl applicationlist state"+applicationlist.size());
			dbc.close();
			return applicationlist;
		}catch(Exception e){
			e.printStackTrace();
		}
		return applicationlist;
		
	}

}
