package com.se.roombooking.db.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Time;
import java.util.LinkedList;

import com.se.roombooking.db.db_conn;
import com.se.roombooking.db.dao.Roomdao;
import com.se.roombooking.vo.Room;

/*
 * if errors occur put //error
 * notice:select 语句不可用 pre-statement
 */

public class Roomdao_impl implements  Roomdao{

	@SuppressWarnings("null")
	@Override
	public LinkedList<Room> get_available_room_bydatetime(java.sql.Date date_beg, java.sql.Date date_end, java.sql.Time time_beg, java.sql.Time time_end) throws Exception {
		// TODO Auto-generated method stub
		db_conn dbc = new db_conn();
		Connection conn = dbc.getConnection();
		PreparedStatement pStatement = null;
		LinkedList<Room> roomlist = new LinkedList<Room>();
		ResultSet rs = null; 
		String select_by_date = "SELECT * FROM `room` WHERE (`dateBeg` <= '"+date_beg+"' and `dateEnd` >= '"+date_end+"') and ('timeBeg' <= '"+time_beg+"' and 'timeEnd' >= '"+time_end+"');";
		try{
			//set user_name to delete
			pStatement = conn.prepareStatement(select_by_date);
		
			try{
			rs = pStatement.executeQuery(select_by_date);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if (rs != null) {
				while (rs.next()) {
					Room room = new Room();
					room.set_room_no(rs.getString("room_no"));
					room.set_date_beg(rs.getDate("dateBeg"));
					room.set_date_end(rs.getDate("dateEnd"));
					room.set_days_of_week(rs.getString("daysOfWeek"));
					room.set_time_beg(rs.getTime("timeBeg"));
					room.set_time_end(rs.getTime("timeEnd"));
					room.set_capacity(rs.getInt("capacity"));
					room.set_facilities(rs.getString("facilities"));
					room.set_hours_count(rs.getInt("hours_count"));
					roomlist.add(room);
				}
			}
			else{
			//if empty, then rs.next() is false
			if (!rs.next()){
				System.out.println("User list empty");
			}
			}
			dbc.close();
			return roomlist;
		}catch(Exception e){
			e.printStackTrace();
		}
		return roomlist;
	}	

	@Override
	public boolean change_one_room_info(String room_no) throws Exception {
		// TODO Auto-generated method stub
		
		
		return false;
	}

	@Override
	public LinkedList<Room> get_room_list()
			throws Exception {
		// TODO Auto-generated method stub
		db_conn dbc = new db_conn();
		Connection conn = dbc.getConnection();
		PreparedStatement pStatement = null;
		LinkedList<Room> roomlist = new LinkedList<Room>();
		ResultSet rs = null; 
		//select 语句不可用 pre-statement
		String select_by_room_no = "SELECT *  FROM `room` WHERE `room_no`;";
		try{
			//set user_name to delete
			pStatement = conn.prepareStatement(select_by_room_no);
			try{
			rs = pStatement.executeQuery(select_by_room_no);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if (rs != null) {
				while (rs.next()) {
					Room room = new Room();
					room.set_room_no(rs.getString("room_no"));
					room.set_date_beg(rs.getDate("dateBeg"));
					room.set_date_end(rs.getDate("dateEnd"));
					room.set_days_of_week(rs.getString("daysOfWeek"));
					room.set_time_beg(rs.getTime("timeBeg"));
					room.set_time_end(rs.getTime("timeEnd"));
					room.set_capacity(rs.getInt("capacity"));
					room.set_facilities(rs.getString("facilities"));
					room.set_hours_count(rs.getInt("hours_count"));
					roomlist.add(room);
				}
			}
			
			//if empty, then rs.next() is false
			else{
			if (!rs.next()){
				System.out.println("User list empty");
			}
			}
			dbc.close();
			return roomlist;
		}catch(Exception e){
			e.printStackTrace();
		}
		return roomlist;
	}

	@SuppressWarnings("null")
	@Override
	public LinkedList<Room> get_available_room_withcapacity(java.sql.Date date_beg,java.sql.Date date_end, 
			java.sql.Time time_beg, java.sql.Time time_end,int capacity) throws Exception {
		// TODO Auto-generated method stub
		db_conn dbc = new db_conn();
		Connection conn = dbc.getConnection();
		PreparedStatement pStatement = null;
		LinkedList<Room> roomlist = new LinkedList<Room>();
		ResultSet rs = null; 
		String select_by_date = "SELECT * FROM `room` WHERE (`dateBeg` <= '"+date_beg+"') " +
				"and (`dateEnd` >= '"+date_end+"')" +
						" and ('timeBeg' <= '"+time_beg+"') " +
								"and ('timeEnd' >= '"+time_end+"') " +
										"and ('capacity' > '"+capacity+"');";
		try{
			//set user_name to delete
			pStatement = conn.prepareStatement(select_by_date);
		
			try{
			rs = pStatement.executeQuery(select_by_date);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if (rs != null) {
				while (rs.next()) {
					Room room = new Room();
					room.set_room_no(rs.getString("room_no"));
					room.set_date_beg(rs.getDate("dateBeg"));
					System.out.println("dateBeg"+room.get_date_beg());
					room.set_date_end(rs.getDate("dateEnd"));
					System.out.println("dateEnd"+room.get_date_end());
					room.set_days_of_week(rs.getString("daysOfWeek"));
					room.set_time_beg(rs.getTime("timeBeg"));
					room.set_time_end(rs.getTime("timeEnd"));
					room.set_capacity(rs.getInt("capacity"));
					room.set_facilities(rs.getString("facilities"));
					room.set_hours_count(rs.getInt("hours_count"));
					roomlist.add(room);
				}
			}
			else{
			//if empty, then rs.next() is false
			if (!rs.next()){
				System.out.println("User list empty");
			}
			}
			dbc.close();
			return roomlist;
		}catch(Exception e){
			e.printStackTrace();
		}
		return roomlist;
	}

	@SuppressWarnings("null")
	@Override
	public LinkedList<Room> get_available_room_withcapfacilities(Date date_beg,
			Date date_end, Time time_beg, Time time_end,int capacity,String facilities) throws Exception {
		// TODO Auto-generated method stub
		db_conn dbc = new db_conn();
		Connection conn = dbc.getConnection();
		PreparedStatement pStatement = null;
		LinkedList<Room> roomlist = new LinkedList<Room>();
		ResultSet rs = null; 
		String select_by_date = "SELECT * FROM `room` WHERE (`dateBeg` <= '"+date_beg+"') " +
				"and (`dateEnd` >= '"+date_end+"') and ('timeBeg' <= '"+time_beg+"') " +
						"and ('timeEnd' >= '"+time_end+"') and ('facilities' LIKE '%"+facilities+"%')" +
						"and ('capacity' > '"+capacity+"');";
		try{
			//set user_name to delete
			pStatement = conn.prepareStatement(select_by_date);
		
			try{
			rs = pStatement.executeQuery(select_by_date);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if (rs != null) {
				while (rs.next()) {
					Room room = new Room();
					room.set_room_no(rs.getString("room_no"));
					room.set_date_beg(rs.getDate("dateBeg"));
					System.out.println("dateBeg"+room.get_date_beg());
					room.set_date_end(rs.getDate("dateEnd"));
					System.out.println("dateEnd"+room.get_date_end());
					room.set_days_of_week(rs.getString("daysOfWeek"));
					room.set_time_beg(rs.getTime("timeBeg"));
					room.set_time_end(rs.getTime("timeEnd"));
					room.set_capacity(rs.getInt("capacity"));
					room.set_facilities(rs.getString("facilities"));
					room.set_hours_count(rs.getInt("hours_count"));
					roomlist.add(room);
				}
			}
			else{
			//if empty, then rs.next() is false
			if (!rs.next()){
				System.out.println("User list empty");
			}
			}
			dbc.close();
			return roomlist;
		}catch(Exception e){
			e.printStackTrace();
		}
		return roomlist;
	}

	@SuppressWarnings("null")
	@Override
	public LinkedList<Room> get_available_room_withfacilities(Date date_beg,
			Date date_end, Time time_beg, Time time_end, String facilities) throws Exception {
		// TODO Auto-generated method stub
		db_conn dbc = new db_conn();
		Connection conn = dbc.getConnection();
		PreparedStatement pStatement = null;
		LinkedList<Room> roomlist = new LinkedList<Room>();
		ResultSet rs = null; 
		String select_by_date = "SELECT * FROM `room` WHERE (`dateBeg` <= '"+date_beg+"') " +
				"and (`dateEnd` >= '"+date_end+"') and ('timeBeg' <= '"+time_beg+"') " +
						"and ('timeEnd' >= '"+time_end+"') and ('facilities')  LIKE '%"+facilities+"%';";
		try{
			//set user_name to delete
			pStatement = conn.prepareStatement(select_by_date);
		
			try{
			rs = pStatement.executeQuery(select_by_date);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if (rs != null) {
				while (rs.next()) {
					Room room = new Room();
					room.set_room_no(rs.getString("room_no"));
					room.set_date_beg(rs.getDate("dateBeg"));
					System.out.println("dateBeg"+room.get_date_beg());
					room.set_date_end(rs.getDate("dateEnd"));
					System.out.println("dateEnd"+room.get_date_end());
					room.set_days_of_week(rs.getString("daysOfWeek"));
					room.set_time_beg(rs.getTime("timeBeg"));
					room.set_time_end(rs.getTime("timeEnd"));
					room.set_capacity(rs.getInt("capacity"));
					room.set_facilities(rs.getString("facilities"));
					room.set_hours_count(rs.getInt("hours_count"));
					roomlist.add(room);
				}
			}
			else{
			//if empty, then rs.next() is false
			if (!rs.next()){
				System.out.println("User list empty");
			}
			}
			dbc.close();
			return roomlist;
		}catch(Exception e){
			e.printStackTrace();
		}
		return roomlist;
	}
	
	public LinkedList<Room> get_room_list_roomno(String room_no) throws Exception{
		db_conn dbc = new db_conn();
		Connection conn = dbc.getConnection();
		PreparedStatement pStatement = null;
		LinkedList<Room> roomlist = new LinkedList<Room>();
		ResultSet rs = null; 
		//select 语句不可用 pre-statement
		String select_by_room_no = "SELECT *  FROM `room` WHERE `room_no`='"+room_no+"';";
		try{
			//set user_name to delete
			pStatement = conn.prepareStatement(select_by_room_no);
			try{
			rs = pStatement.executeQuery(select_by_room_no);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if (rs != null) {
				while (rs.next()) {
					Room room = new Room();
					room.set_room_no(rs.getString("room_no"));
					room.set_date_beg(rs.getDate("dateBeg"));
					room.set_date_end(rs.getDate("dateEnd"));
					room.set_days_of_week(rs.getString("daysOfWeek"));
					room.set_time_beg(rs.getTime("timeBeg"));
					room.set_time_end(rs.getTime("timeEnd"));
					room.set_capacity(rs.getInt("capacity"));
					room.set_facilities(rs.getString("facilities"));
					room.set_hours_count(rs.getInt("hours_count"));
					roomlist.add(room);
				}
			}
			
			//if empty, then rs.next() is false
			else{
			if (!rs.next()){
				System.out.println("User list empty");
			}
			}
			dbc.close();
			return roomlist;
		}catch(Exception e){
			e.printStackTrace();
		}
		return roomlist;
	}

}
