package com.se.roombooking.vo;

public class User {
	private int user_id;
	private String username;
	private String password;
	private int property;
	
	public int get_user_id(){
		return user_id;
	} 
	
	public String get_username(){
		return username;
	}
	
	public String get_password(){
		return password;
	}
	
	public int get_property(){
		return property;
	}
	
	public void set_user_id(int user_id){
		this.user_id = user_id;
	}
	
	public void set_username(String username){
		this.username = username;
	}
	
	public void set_password(String password){
		this.password = password;
	}
	
	public void set_property(int property){
		this.property = property;
	}
}
