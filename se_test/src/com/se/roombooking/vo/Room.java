package com.se.roombooking.vo;

import java.sql.Date;
import java.sql.Time;

public class Room {
	private String room_no;
	private Date date_beg;
	private Date date_end;
	private Time time_beg;
	private Time time_end;
	private String days_of_week;
	private int capacity;
	private String facilities;
	private int room_state;
	//hours requesting in whole
	private int hours_count;
	
	public int get_hours_count(){
		return hours_count;
	}
	
	public String get_room_no(){
		return room_no;
	}
	
	public Date get_date_beg(){
		return date_beg;
	}
	
	public Date get_date_end(){
		return date_end;
	}
	
	public Time get_time_beg(){
		return time_beg;
	}
	
	public Time get_time_end(){
		return time_end;
	}
	
	public String get_days_of_week(){
		return days_of_week;
	}
	
	public int get_capacity(){
		return capacity;
	}
	
	public String get_facilities(){
		return facilities;
	}
	
	public int get_room_state(){
		return room_state;
	}
	
	public void set_room_no(String room_no){
		this.room_no = room_no;
	}
	
	public void set_date_beg(Date date_beg){
		this.date_beg = date_beg;
	}
	
	public void set_date_end(Date date_end){
		this.date_end = date_end;
	}
	
	public void set_time_beg(Time time_beg){
		this.time_beg = time_beg;
	}
	
	public void set_time_end(Time time_end){
		this.time_end = time_end;
	}
	
	public void set_days_of_week(String days_of_week){
		this.days_of_week = days_of_week;
	}
	
	public void set_capacity(int capacity){
		this.capacity = capacity ;
	}
	
	public void set_facilities(String facilities){
		this.facilities = facilities;
	}
	
	
	public void set_room_state(int room_state){
		this.room_state = room_state ;
	}
	
	public void set_hours_count(int hours_count){
		this.hours_count = hours_count ;
	}
}
