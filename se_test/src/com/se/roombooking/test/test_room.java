package com.se.roombooking.test;

import java.util.LinkedList;
import com.se.roombooking.db.dao.impl.Roomdao_impl;
import com.se.roombooking.vo.Room;

public class test_room {
	public static void main(String[] args) throws Exception{
		Roomdao_impl rdi = new Roomdao_impl();
		LinkedList<Room> roomlist = new LinkedList<Room>();
		String try_date = "2011-12-01";
		String try_date1 = "2011-12-03";
		String try_time = "13:00:00";
		String try_time1 = "15:00:00";
		int capacity = 10;
		String facilities = "comp";
		Room room = new Room();
		room.set_room_no("101");
		java.sql.Date sql_date = java.sql.Date.valueOf(try_date);
		java.sql.Date sql_date1 = java.sql.Date.valueOf(try_date1);
		java.sql.Time sql_time =  java.sql.Time.valueOf(try_time);
		java.sql.Time sql_time1 = java.sql.Time.valueOf(try_time1);
		
		System.out.print("get all room\n");
		roomlist = rdi.get_room_list();
		print_application_list(roomlist);
		
		System.out.print("get room by room_no\n");
		roomlist = rdi.get_room_list_roomno(room.get_room_no());
		print_application_list(roomlist);
		
		System.out.print("get room with date and time\n");
		roomlist = rdi.get_available_room_bydatetime(sql_date,sql_date1,sql_time,sql_time1);
		print_application_list(roomlist);
	
		System.out.print("get room with date and time and capacity\n");
		roomlist = rdi.get_available_room_withcapacity(sql_date,sql_date1,sql_time,sql_time1,capacity);
		print_application_list(roomlist);
		
		System.out.print("get room with date and time and facilities\n");
		roomlist = rdi.get_available_room_withcapfacilities(sql_date,sql_date1,sql_time,sql_time1,capacity,facilities);
		print_application_list(roomlist);
		
		System.out.print("get room with date and time and capacity and facilities\n");
		roomlist = rdi.get_available_room_withfacilities(sql_date,sql_date1,sql_time,sql_time1,facilities);
		print_application_list(roomlist);
		
		
		//test date and time operation
		
		/*
		String try_date = "2005-9-1";
		String try_date1 = "2005-9-3";
		String try_time = "11:00:00";
		String try_time1 = "18:00:00";
		
		java.sql.Time sql_time = java.sql.Time.valueOf(try_time);
		java.sql.Time sql_time1 = java.sql.Time.valueOf(try_time1);
		java.sql.Date sql_date = java.sql.Date.valueOf(try_date);
		java.sql.Date sql_date1 = java.sql.Date.valueOf(try_date1);
		System.out.println(sql_time+""+sql_time1);
		System.out.println(time_after(sql_time,sql_time1));
		System.out.println(sql_date+""+sql_date1);
		System.out.println(date_after(sql_date,sql_date1));
		 */
	}
	private static void print_application_list(LinkedList<Room> roomlist){
		int counterMinus = roomlist.size() - 1;
		//System.out.print(roomlist.size());
		while (counterMinus >= 0) {
			Room room_receive = roomlist.get(counterMinus);
			System.out.print("End"+room_receive.get_date_end()+"\nBeg"+room_receive.get_date_beg()+"\n");
			counterMinus--;
		}
	
	}
	
}
