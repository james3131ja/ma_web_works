package com.se.roombooking.test;

import java.util.LinkedList;

import com.se.roombooking.db.dao.impl.Roomdao_impl;
import com.se.roombooking.vo.Room;

public class testgetavaiavleroom {
	public static void main(String[] args) throws Exception{
		Room room  = new Room();
		room.set_date_beg(null);
		room.set_date_end(null);
		room.set_time_beg(null);
		room.set_time_end(null);
		room.set_room_no("");
		
		Roomdao_impl rdi = new Roomdao_impl();
		LinkedList<Room> roomlsit = new LinkedList<Room>();
		try{
			System.out.println("testroomlist: get all room list\n");
			print_user_list(rdi.get_available_room_bydatetime
					(room.get_date_beg(),room.get_date_end(),room.get_time_beg(),room.get_time_end()));
		}catch(Exception e){
			e.printStackTrace();
		}
		
		try{
			System.out.println("testroomlist: get room list with capacity\n");
			print_user_list(rdi.get_available_room_withcapacity(room.get_date_beg(), room.get_date_end(), room.get_time_beg(),room.get_time_end(),room.get_capacity()));
		}catch(Exception e){
			e.printStackTrace();
		}
		try{
			System.out.println("testroomlist: get room list with capacity and facilities\n");
			roomlsit = rdi.get_available_room_withcapfacilities(room.get_date_beg(),room.get_date_end(),room.get_time_beg(),room.get_time_end(),room.get_capacity(), room.get_facilities());	
			print_user_list(roomlsit);
		}catch(Exception e){
			e.printStackTrace();
		}
		
	
	}
	
	
	private static void print_user_list(LinkedList<Room> roomlsit){
		int counterMinus = roomlsit.size() - 1;
		while (counterMinus >= 0) {
			Room room_list_receive = roomlsit.get(counterMinus);
			System.out.print(room_list_receive.get_room_no()
					+ room_list_receive.get_date_beg()
					+ room_list_receive.get_date_end()
					+ room_list_receive.get_days_of_week()
					+ room_list_receive.get_time_beg()
					+ room_list_receive.get_time_end()
					+ room_list_receive.get_room_state()
					+ room_list_receive.get_capacity()
					+ room_list_receive.get_facilities()
					+ room_list_receive.get_hours_count()
					+"\n");
			counterMinus--;
		}
	}
}
