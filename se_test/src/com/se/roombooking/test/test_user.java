package com.se.roombooking.test;

import java.util.LinkedList;

import com.se.roombooking.db.dao.impl.Userdao_impl;
import com.se.roombooking.vo.User;

public class test_user {
	public static void main(String[] args) throws Exception{
		User user = new User();
		user.set_username("22");
		user.set_password("15");
		user.set_property(3);
		Userdao_impl udi = new Userdao_impl();
		
		//add user
		try {
			if (udi.add_user(user)) {
				System.out.println("test user: Insert user success!");
			} else {
				System.out.println("cao! 老子又进不去！");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//delete user
		user.set_username("22");
		try {
			if (udi.delete_user(user.get_username())) {
				System.out.println("test user::Delete user success!");
			} else {
				System.out.println("cao! 老子又进不去！");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//user list
		LinkedList<User> userlist= new LinkedList<User>();
		try{
			userlist = udi.get_user_list();
			System.out.println("test user:: get list success!");
			print_user_list(userlist);
		}catch(Exception e){
			e.printStackTrace();
		}
		//validate user
		user.set_username("1");
		try {
			if (udi.validate_database_info(user.get_username())) {
				System.out.println("test user::validate user in database!");
			} else {
				System.out.println("cao! 老子又进不去！");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//valid login
		user.set_username("15");
		user.set_password("1");
		try {
			if (udi.validate_user(user.get_username(),user.get_password())) {
				System.out.println("test user: user exist and return info!");
			} else {
				System.out.println("test user:cao! 老子又进不去！");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//get_user_byname
		user.set_username("15");
		try {
			if (udi.validate_database_info(user.get_username())) {
				System.out.print("Userdao_impl:"+udi.get_user_byname(user.get_username()).get_property()+"\n");
			} else {
				System.out.println("test user:cao! 老子又进不去！");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}//static void main
	
	//print list
	private static void print_user_list(LinkedList<User> userlist){
		int counterMinus = userlist.size() - 1;
		while (counterMinus >= 0) {
			User user_receive = userlist.get(counterMinus);
			System.out.print(user_receive.get_username()
					+ user_receive.get_password()
					+ user_receive.get_property()+"\n");
			counterMinus--;
		}
 }//public class
}//test user
