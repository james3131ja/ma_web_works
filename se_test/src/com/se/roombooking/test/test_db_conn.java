package com.se.roombooking.test;
import com.se.roombooking.db.db_conn;
import java.sql.*;

public class test_db_conn {
	public static void main(String[] args) throws Exception{
		db_conn dbc = new db_conn();
		Connection conn = dbc.getConnection();
		
		if (conn == null){System.out.println("not connected");}
		else{
			System.out.println(conn);
		}
	}
}
