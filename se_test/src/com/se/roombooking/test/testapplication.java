package com.se.roombooking.test;



import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import com.se.roombooking.db.dao.impl.Applicationdao_impl;
import com.se.roombooking.vo.Application;


public class testapplication {
	public static void main(String[] args) throws Exception{
		Applicationdao_impl adi = new Applicationdao_impl();
		LinkedList<Application> applicationlist = new LinkedList<Application>();
		
		
		
		System.out.print("test String array\n");
		String[] str = new String[2];
		str[0]="11";
		str[1]="22";
		System.out.print(str[0]+str[1]+"\n");
		
		System.out.print("test update state\n");
		try{
			adi.modify_application_state(1, 1);
			}catch(Exception e){
				System.out.print("111111111111111111");
			}
		
		System.out.print("test application all list\n");
		try{
		applicationlist = adi.get_application_list();
		}catch(Exception e){
			System.out.print("111111111111111111");
		}
		print_application_list(applicationlist);
	    Application application = new Application();
	    
	    
	    System.out.print("test application list with state\n");
		try{
		print_application_list(adi.get_application_list_withstate(1));
		}catch(Exception e){
			System.out.print("22222222222222222222222");
		}
	    
		System.out.print("test delete application witt application id\n");
		try{
			adi.delete_application(application.get_applicant_id());
			}catch(Exception e){
				e.printStackTrace();
			}
				
		System.out.print("test list application with state\n");
		try{
				print_application_list(adi.get_application_list_withstate(1));
				}catch(Exception e){
				e.printStackTrace();
			}		
				
				application.set_applicant_id(3);
				application.set_applicant_name("12");
				application.set_room_no("12");
				application.set_date_beg(null);
				application.set_date_end(null);
				application.set_days_of_week("1");
				application.set_time_beg(null);
				application.set_time_end(null);
				application.set_room_state(0);
				application.set_capacity(10);
				application.set_facilities(null);
		
				System.out.print("test list add application\n");		
		try{
			adi.add_application(application);
			}catch(Exception e){
			e.printStackTrace();
		}	
			
		
		//test date and time operation
		String try_date = "2011-12-1";
		String try_date1 = "2010-12-5";
		String try_time = "11:00:00";
		String try_time1 = "18:00:00";
		
		java.sql.Time sql_time = java.sql.Time.valueOf(try_time);
		java.sql.Time sql_time1 = java.sql.Time.valueOf(try_time1);
		java.sql.Date sql_date = java.sql.Date.valueOf(try_date);
		java.sql.Date sql_date1 = java.sql.Date.valueOf(try_date1);
		 
		
		System.out.println(sql_time+""+sql_time1);
		System.out.println(time_after(sql_time,sql_time1));
		System.out.println(sql_date+""+sql_date1);
		System.out.println(date_after(sql_date,sql_date1));

		System.out.print("test get day\n");
		System.out.println(getDay(try_date));
		
		System.out.print("test get days\n");
		System.out.println(getNumOfDays(try_date,try_date1));
	}
	
	private static boolean time_after(java.sql.Time t1 , java.sql.Time t2){
		boolean isSuccess = false;
		if(t1.after(t2)){
			isSuccess = true;
		}
		return isSuccess;
	}
	
	private static boolean date_after(java.sql.Date d1,java.sql.Date d2){
		boolean isSuccess = false;
		if(d1.after(d2)){
			isSuccess = true;
		}
		return isSuccess;
	}
	
	private static void print_application_list(LinkedList<Application> applicationlist){
		int counterMinus = applicationlist.size() - 1;
		while (counterMinus >= 0) {
			Application user_receive = applicationlist.get(counterMinus);
			System.out.print(user_receive.get_applicant_id()+"\n");
			counterMinus--;
		}
	}
	
	private static int getDay(String date) {
		int a = 7;
		int y, m, d;
		String aDate[] = new String[3];
		aDate = date.split("-");
		y = Integer.parseInt(aDate[0]);
		m = Integer.parseInt(aDate[1]);
		d = Integer.parseInt(aDate[2]);
		if ((m == 1) || (m == 2)) {
			m += 12;
			y--;
		}
		a = ((d + 2 * m + 3 * (m + 1) / 5 + y + y / 4 - y / 100 + y / 400) % 7) + 1;
		return a;
	}
	
	private static int getNumOfDays(String dateFrom, String dateTo) {
		DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		Date date2 = null;
		try {
			date = format1.parse(dateFrom);
			date2 = format1.parse(dateTo);
			Calendar aCalendar = Calendar.getInstance();

			aCalendar.setTime(date);

			int day1 = aCalendar.get(Calendar.DAY_OF_YEAR);

			aCalendar.setTime(date2);

			int day2 = aCalendar.get(Calendar.DAY_OF_YEAR);

			int day = day2 - day1;
			if (day < 0) {
				return -day;
			} else {
				return day;
			}

		} catch (ParseException e) {
			e.printStackTrace();
			return 0;
		}
	}
	//date operation
	/*
	String try_date = "2005-9-6";
	SimpleDateFormat dateFM = new SimpleDateFormat("yyyy-MM-dd");
	java.util.Date date = dateFM.parse(try_date);
	System.out.print(dateFM.format(date));    //�������ǣ�2005-2-19
	
	java.sql.Date sql_date = java.sql.Date.valueOf(try_date);
	System.out.println(sql_date+"*"+sql_date.getYear()+"*"+sql_date.getMonth()+"*"+sql_date.getDay());  
	*/
	
}
